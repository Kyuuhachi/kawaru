rootProject.name = "minecraft-mods"

includeBuild("takara")
includeBuild("tsukuro")

include("minecraft")
project(":minecraft").buildFileName = "../minecraft.gradle.kts"

val mcMods: String by settings

mcMods.split(",").forEach { it ->
	include(it)
	project(":$it").buildFileName = "../mods.gradle.kts"
}
