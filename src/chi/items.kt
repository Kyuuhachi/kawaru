package kawaru.chi

import org.lwjgl.opengl.GL11.*

import mc.client.gui.GuiComponent
import mc.client.renderer.texture.TextureAtlas
import mc.resources.ResourceLocation
import mc.world.effect.MobEffectUtil
import mc.world.entity.player.Player
import mc.world.item.ItemStack
import mc.world.item.ProjectileWeaponItem
import mc.world.item.SpectralArrowItem
import mc.world.item.alchemy.PotionUtils

import kawaru.kokoro.util.*
import kawaru.kokoro.util.draw.*
import kawaru.kokoro.util.text.*

object Items {
	val INVENTORY = ResourceLocation("textures/gui/container/inventory.png")

	fun renderOverlay(pose: PoseStack, i: ItemStack, x: Int, y: Int) {
		drawAll(pose, i, x, y, OverlayMode.TRUE)
	}

	fun renderHotbar(pose: PoseStack, pl: Player, i: ItemStack, x: Int, y: Int) {
		if(pl.handSlots.any{i === it})
			drawAll(pose, i, x, y, OverlayMode.HELD)
	}

	private fun drawAll(pose: PoseStack, i: ItemStack, x: Int, y: Int, mode: OverlayMode) {
		glPushAttrib {
			glDisable(GL_LIGHTING)
			glDisable(GL_DEPTH_TEST)
			glDisable(GL_BLEND)
			glEnable(GL_ALPHA_TEST)
			pose {
				pose.translate(x, y)
				pose.scale(0.5, 0.5)
				if(config.items.damage == mode) drawDamage(pose, i)
				if(config.items.potion == mode) drawEffects(pose, i)
				if(config.items.ammo == mode) drawAmmo(pose, i)
			}
		}
	}

	fun drawDamage(pose: PoseStack, i: ItemStack) {
		if(i.isDamaged) {
			val h = (i.maxDamage-i.damageValue) / i.maxDamage.𝒻
			FONT.outline(pose, 𝕊(engNotation((i.maxDamage-i.damageValue).𝓁)), 32, 32, RGBA(1f-h, h, 0f), xalign=1f, yalign=1f)
		}
	}

	fun drawEffects(pose: PoseStack, i: ItemStack) {
		val effects = PotionUtils.getMobEffects(i).also { if(it.none()) return }
		val time = MC.level?.gameTime ?: return

		val e = effects[time.𝒾/30%effects.size]
		val p = e.effect

		glColor3f(1f, 1f, 1f)
		glEnable(GL_ALPHA_TEST)

		if(e.showIcon()) {
			val sprite = MC.mobEffectTextures[p]
			MC.textureManager.bind(sprite.atlas().location())
			GuiComponent.blit(pose, 0, 0, 0, 18, 18, sprite)
		}

		FONT.outline(pose, 𝕋("potion.potency.${e.amplifier}") { merge(p.category.tooltipFormatting) }, 9, 3, WHITE, xalign=0.5f)

		if(!p.isInstantenous)
			FONT.outline(pose, 𝕊(MobEffectUtil.formatDuration(e, 1f)), 32, 32, WHITE, xalign=1f, yalign=1f)
	}

	fun drawAmmo(pose: PoseStack, i: ItemStack) {
		val item = i.item
		if(item is ProjectileWeaponItem) {
			val arrow = ProjectileWeaponItem.getHeldProjectile(PLAYER, item.allSupportedProjectiles)
			if(arrow.isEmpty()) return
			val count = (0 until PLAYER.inventory.containerSize)
				.map {PLAYER.inventory.getItem(it)}
				.filter {ItemStack.tagMatches(arrow, it)}
				.sumBy {it.count}

			if(arrow.item is SpectralArrowItem)
				FONT.outlineCustom(pose, 𝕊(count), 30, 2, 0xFFFF5F.rgb, 0x7F7F3F.rgb, xalign=1f)
			else
				FONT.outline(pose, 𝕊(count), 30, 2, MC.itemColors.getColor(arrow, 0).rgb, xalign=1f)
		}
	}
}

fun engNotation(n: Long, places: Int = 5) =
	((n.toString().length - places)/3).let { when {
		it <= 0 -> "${n}"
		it <= 1 -> "${n/1000}K"
		it <= 2 -> "${n/1000_000}M"
		it <= 3 -> "${n/1000_000_000}G"
		it <= 4 -> "${n/1000_000_000_000}T"
		it <= 5 -> "${n/1000_000_000_000_000}P"
		it <= 6 -> "${n/1000_000_000_000_000_000}E"
		else -> throw UnsupportedOperationException()
	} }
