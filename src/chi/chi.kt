package kawaru.chi

import kawaru.kokoro.config.*

data class ChiConfig(val tooltip : Tooltip, val hud : Hud, val items : Items, val gui : Gui) {
	data class Tooltip(val experience : Boolean, val food : Boolean, val furnace : Boolean)
	data class Hud(val potion : Boolean, val experience : Boolean, val horse : Boolean, val saturation : Boolean, val clock : Boolean)
	data class Items(val damage : OverlayMode, val potion : OverlayMode, val ammo : OverlayMode, val coloredSlots : Boolean)
	data class Gui(val fuel : Boolean)
}

enum class OverlayMode { TRUE, FALSE, HELD }

val config = Config("kawaru-chi").asMap().let { ChiConfig(
	tooltip=it["tooltip"]?.asMap().orEmpty().let { ChiConfig.Tooltip(
		experience=it["experience"]?.asBool() ?: true,
		food=it["food"]?.asBool() ?: true,
		furnace=it["furnace"]?.asBool() ?: true
	) },
	hud=it["hud"]?.asMap().orEmpty().let { ChiConfig.Hud(
		potion=it["potion"]?.asBool() ?: true,
		experience=it["experience"]?.asBool() ?: true,
		horse=it["horse"]?.asBool() ?: true,
		saturation=it["saturation"]?.asBool() ?: true,
		clock=it["clock"]?.asBool() ?: true
	) },
	items=it["items"]?.asMap().orEmpty().let { ChiConfig.Items(
		damage=it["damage"]?.asEnum<OverlayMode>() ?: OverlayMode.TRUE,
		potion=it["potion"]?.asEnum<OverlayMode>() ?: OverlayMode.TRUE,
		ammo=it["ammo"]?.asEnum<OverlayMode>() ?: OverlayMode.HELD,
		coloredSlots=it["colored_slots"]?.asBool() ?: true
	) },
	gui=it["gui"]?.asMap().orEmpty().let { ChiConfig.Gui(
		fuel=it["fuel"]?.asBool() ?: true
	) }
) }
