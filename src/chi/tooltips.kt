package kawaru.chi

import mc.world.entity.player.Player
import mc.world.item.Items
import mc.world.inventory.AbstractFurnaceMenu
import mc.world.item.ItemStack
import mc.world.item.crafting.AbstractCookingRecipe
import mc.world.item.crafting.RecipeManager
import mc.world.effect.MobEffectUtil
import mc.network.chat.Component

import kawaru.kokoro.util.*
import kawaru.kokoro.util.text.*

private fun formatTime(s : String, t : Int) = 𝕋(if(t == 20) s+".1s" else s, (t/20f).toString("#.#"))

object Tooltips {
	fun addFoodInfo(i : ItemStack, list : MutableList<Component>) {
		if(!config.tooltip.food) return
		val food = i.item.foodProperties ?: return
		list.add(ℕ)
		list.add(𝕋("kawaru-chi:food.nutrition", food.nutrition))
		list.add(𝕋("kawaru-chi:food.saturation", (food.saturationModifier * food.nutrition * 2).toString("#.##")))
		if(food.isFastFood) list.add(𝕋("kawaru-chi:food.fast"))
		food.effects.forEach{ (eff, chance) ->
			val k = StringBuilder("kawaru-chi:food.effect")
			if(chance != 1f) k.append(".chance")
			if(eff.amplifier != 0) k.append(".level")
			list.add(𝕋(k.toString(),
				eff.effect.displayName,
				𝕋("potion.potency.${eff.amplifier}"),
				MobEffectUtil.formatDuration(eff, 1f),
				(chance * 100).toString("#")
			) { merge(eff.effect.category.tooltipFormatting) })
		}
		if(food.canAlwaysEat()) list.add(𝕋("kawaru-chi:food.always"))
		if(food.isMeat) list.add(𝕋("kawaru-chi:food.meat"))
	}
}
