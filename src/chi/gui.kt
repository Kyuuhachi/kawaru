package kawaru.chi

import mc.network.chat.Component
import mc.client.gui.screens.inventory.CreativeModeInventoryScreen
import mc.client.gui.screens.inventory.InventoryScreen
import mc.client.gui.screens.inventory.AbstractFurnaceScreen
import mc.world.item.CreativeModeTab
import mc.world.item.crafting.AbstractCookingRecipe
import com.mojang.blaze3d.vertex.PoseStack

import kawaru.kokoro.util.*
import kawaru.kokoro.util.draw.*
import kawaru.kokoro.util.text.*

object GUI {
	fun InventoryScreen.drawXpStats(pose: PoseStack, mx : Int, my : Int) {
		if(!config.tooltip.experience) return
		if(mx - this.leftPos in 28..75 && my - this.topPos in 8..78)
			this.renderComponentTooltip(pose, getXpInfo(), mx, my)
	}

	fun CreativeModeInventoryScreen.drawXpStats(pose: PoseStack, mx : Int, my : Int) {
		if(!config.tooltip.experience) return
		if(mx - this.leftPos in 73..105 && my - this.topPos in 6..49 && CreativeModeInventoryScreen.selectedTab == CreativeModeTab.TAB_INVENTORY.id)
			this.renderComponentTooltip(pose, getXpInfo(), mx, my)
	}

	fun AbstractFurnaceScreen<*>.renderFurnaceInfo(pose: PoseStack) {
		if(!config.gui.fuel) return
		val fields = this.menu.data
		if(fields[0] != 0) {
			val input = this.menu.container.getItem(0)
			LEVEL.recipeManager.recipes
				.mapNotNull { it as? AbstractCookingRecipe }
				.filter { it.type == this.menu.recipeType }
				.firstOrNull{ it.ingredients.single().test(input) }
				?.let{
					val n = (fields[0] + fields[2]) / it.cookingTime
					FONT.shadow(pose, 𝕊(n), this.leftPos+56+7, this.topPos+36+7, WHITE, xalign=.5f, yalign=.5f)
				}
		}
	}
}

private fun getXpInfo() : List<Component> {
	val level = PLAYER.experienceLevel
	val total = PLAYER.totalExperience
	val cap = PLAYER.xpNeededForNextLevel
	val l = level / 15
	return listOf(
		𝕋("kawaru-chi:experience.level", 𝕊(level) { color = YELLOW }),
		𝕋("kawaru-chi:experience.total", 𝕊(total) { color = YELLOW }),
		ℕ,
		𝕋("kawaru-chi:experience.next", 𝕊(cap) { color = YELLOW }),
		𝕋("kawaru-chi:experience.next.30") { color = if(l >= 2) GRAY else DARK_GRAY },
		𝕋("kawaru-chi:experience.next.15") { color = if(l == 1) GRAY else DARK_GRAY },
		𝕋("kawaru-chi:experience.next.00") { color = if(l == 0) GRAY else DARK_GRAY },
		ℕ,
		𝕋("kawaru-chi:experience.dropped", 𝕊((level*7).coerceAtMost(100)) { color = YELLOW }),
		𝕋("kawaru-chi:experience.dropped.equation",
			𝕋("kawaru-chi:experience.dropped.equation.variable") { if(l == 0) color = GRAY },
			𝕋("kawaru-chi:experience.dropped.equation.constant") { if(l >= 1) color = GRAY }
		) { color = DARK_GRAY }
	)
}
