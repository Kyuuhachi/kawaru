package kawaru.chi

import java.lang.reflect.Field
import java.lang.invoke.MethodHandle
import java.lang.invoke.MethodHandles

import com.mojang.blaze3d.vertex.DefaultVertexFormat
import mc.client.renderer.DimensionSpecialEffects
import mc.resources.ResourceLocation
import mc.util.Mth
import mc.world.effect.MobEffectUtil
import mc.world.effect.MobEffects
import mc.world.entity.animal.horse.AbstractHorse

import kawaru.kokoro.util.*
import kawaru.kokoro.util.draw.*
import kawaru.kokoro.util.text.*

object HUD {
	private var inited = false
	private lateinit var f_alpha : MethodHandle
	private lateinit var f_x : MethodHandle
	private lateinit var f_y : MethodHandle
	fun effects(pose: PoseStack, lambdas : List<Any>) {
		// Don't want to copy the positioning code, so I'll just grab some upvalues from the lambdas
		if(!config.hud.potion) return
		if(!inited) lambdas.firstOrNull()?.let {
			val fields = it::class.java.declaredFields
			val lookup = MethodHandles.lookup()::unreflectGetter
			f_alpha = lookup(fields[2].apply{isAccessible=true})
			f_x = lookup(fields[4].apply{isAccessible=true})
			f_y = lookup(fields[5].apply{isAccessible=true})
			inited = true
		}
		PLAYER.activeEffects
			.filter{it.showIcon()}
			.sortedByDescending{it}
			.zip(lambdas)
			.forEach{ (eff, lambda) ->
				val alpha = f_alpha(lambda) as Float
				val x = f_x(lambda) as Int
				val y = f_y(lambda) as Int
				pose {
					pose.translate(x, y)
					pose.scale(0.5, 0.5)
					if(!eff.isAmbient)
						FONT.shadow(pose,  𝕊(MobEffectUtil.formatDuration(eff, 1f)), (25-3)*2, (25-3)*2, RGBA(0.5f, 0.5f, 0.5f, alpha), xalign=1f, yalign=1f)
					if(eff.amplifier > 0)
						FONT.shadow(pose, 𝕋("potion.potency.${eff.amplifier}"), (3)*2, (3)*2, WHITE.copy(a=alpha))
				}
			}
	}

	fun expBar(pose: PoseStack) {
		if(!config.hud.experience) return
		val height = MC.window.guiScaledHeight
		val width = MC.window.guiScaledWidth

		FONT.outlineCustom(pose, 𝕋("kawaru-chi:bar.experience.progress",
			(PLAYER.experienceProgress * PLAYER.xpNeededForNextLevel).𝒾,
			PLAYER.xpNeededForNextLevel
		), width/2 - 182/3, height-35, RGBA(0.5f, 1f, 0f), BLACK, xalign=1/3f)

		FONT.outlineCustom(pose, 𝕋("kawaru-chi:bar.experience.remainder",
			((1 - PLAYER.experienceProgress) * PLAYER.xpNeededForNextLevel).𝒾
		), width/2 + 182/3, height-35, RGBA(0.5f, 1f, 0f), BLACK, xalign=2/3f)
	}

	fun jumpBar(pose: PoseStack) {
		if(!config.hud.horse) return
		fun getHeight(v : Double) = generateSequence(v) { (it - 0.08) * 0.98 }.takeWhile { it > 0 }.sum()

		val height = MC.window.guiScaledHeight
		val width = MC.window.guiScaledWidth

		val horse = PLAYER.vehicle as? AbstractHorse ?: return
		val charge = PLAYER.jumpRidingScale
		val strength = horse.customJump

		FONT.outlineCustom(pose, 𝕋("kawaru-chi:bar.horse.jump",
			getHeight(strength * charge).toString("0.0#"),
			getHeight(strength).toString("0.0#")
		), width/2, height-35, RGBA(1f, 0.5f, 1f), BLACK, xalign=1/2f)
	}

	fun drawSaturation(pose: PoseStack, n : Int, x : Int, y : Int) {
		if(!config.hud.saturation) return
		val sat = Mth.floor(PLAYER.foodData.saturationLevel)
		render({ blend(GL.SRC_ALPHA, GL.ONE) }) {
			var u = if(PLAYER.hasEffect(MobEffects.SATURATION)) 106 else 70
			if(n * 2 + 1 < sat) drawRect(pose, x, y, u, 27, 9, 9)
			if(n * 2 + 1 == sat) drawRect(pose, x, y, u + 9, 27, 9, 9)
		}
	}
}

object Clock {
	val MOON = ResourceLocation("textures/environment/moon_phases.png")
	val SUN = ResourceLocation("textures/environment/sun.png")
	val HUD = ResourceLocation("kawaru-chi", "hud.png")

	fun draw(pose: PoseStack) {
		if(!config.hud.clock) return
		val x = MC.window.guiScaledWidth / 2

		render({ texture(HUD) }) {
			drawRect(pose, x-18, 0, 0, 2, 36, 16)
		}

		val skyColor = RGBA(LEVEL.getSkyColor(MC.gameRenderer.mainCamera.blockPosition, 1f))
		render({ color(skyColor) + !texture }) {
			drawRect(pose, x-14, 2, 0, 0, 28, 10)
		}

		if(LEVEL.effects().skyType() == DimensionSpecialEffects.SkyType.NORMAL) {
			val ang = LEVEL.getTimeOfDay(1f)

			LEVEL.effects().getSunriseColor(ang, 1f)?.let{ (r, g, b, a) ->
				render({ !texture + blend.default + !cull + smooth }) {
					val side = if(ang % 1 < 0.5) -14.0 else 14.0
					tessellate(pose, DefaultVertexFormat.POSITION_COLOR) {
						vertex(x+side*(1-a),  2.0, 0.0).color(r, g, b, 0f).end
						vertex(x+side*(1-a), 12.0, 0.0).color(r, g, b, 0f).end
						vertex(x+side, 12.0, 0.0).color(r, g, b, a).end
						vertex(x+side,  2.0, 0.0).color(r, g, b, a).end
					}
				}
			}

			render({ blend(GL.SRC_ALPHA, GL.ONE, GL.ONE, GL.ZERO) }) {
				render({ texture(SUN) }) {
					drawSunMoon(pose, x, ang+1/2f, {it}, {it})
				}
				render({ texture(MOON) }) {
					val phase = LEVEL.moonPhase
					drawSunMoon(pose, x, ang, {(it+phase%4)/4}, {(it+phase/4)/2})
				}
			}
		}

		val time = (LEVEL.dayTime + 6000) % 24000
		val timeString = "%02d:%02d".format(time / 1000, time % 1000 * 60 / 1000)
		FONT.shadow(pose, 𝕊(timeString), x, 3, WHITE, xalign=0.5f)
	}

	private inline fun drawSunMoon(pose: PoseStack, x : Int, p : Float, uf : (Float) -> Float, vf : (Float) -> Float) {
		val p2 = (1/2f-p%1)*28*2
		val l = (p2-16f).coerceAtLeast(-14f)
		val r = (p2+16f).coerceAtMost(14f)
		val ul = ((16-14-p2)/32f).coerceIn(0f, 1f)
		val ur = ((16+14-p2)/32f).coerceIn(0f, 1f)

		tessellate(pose, DefaultVertexFormat.POSITION_TEX) {
			vertex(x+l.𝒹,  2.0, 0.0).uv(uf(ul), vf(11/32f)).end
			vertex(x+l.𝒹, 12.0, 0.0).uv(uf(ul), vf(21/32f)).end
			vertex(x+r.𝒹, 12.0, 0.0).uv(uf(ur), vf(21/32f)).end
			vertex(x+r.𝒹,  2.0, 0.0).uv(uf(ur), vf(11/32f)).end
		}
	}
}
