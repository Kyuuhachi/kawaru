package kawaru.chi

import mc.client.gui.screens.inventory.CreativeModeInventoryScreen
import mc.world.inventory.Slot
import mc.world.item.CreativeModeTab
import mc.util.Mth

import org.lwjgl.opengl.GL11.*
import org.lwjgl.opengl.GL12.*
import org.lwjgl.opengl.GL13.*
import org.lwjgl.opengl.GL14.*

import kawaru.kokoro.util.draw.*

object Slots {
	fun drawSlot(slot: Slot, pose: PoseStack) {
		if(!config.items.coloredSlots) return
		slot.item.item.itemCategory?.let{ group ->
			glPushAttrib {
				glDisable(GL_TEXTURE_2D)

				initBlend(group)
				drawRect(pose, slot.x, slot.y, 0, 0, 16, 16)
			}
		}
	}

	fun drawTab(group: CreativeModeTab, pose: PoseStack, x: Int, y: Int, u: Int, v: Int, w: Int, h: Int) {
		drawRect(pose, x, y, u, v, w, h)
		if(!config.items.coloredSlots) return

		if(group.id == CreativeModeInventoryScreen.selectedTab || group.isAlignedRight) {
			return
		}

		glPushAttrib {
			glTexEnvi(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_COMBINE)

			glTexEnvi(GL_TEXTURE_ENV, GL_COMBINE_RGB, GL_REPLACE)
			glTexEnvi(GL_TEXTURE_ENV, GL_SOURCE0_RGB, GL_PRIMARY_COLOR)
			glTexEnvi(GL_TEXTURE_ENV, GL_OPERAND0_RGB, GL_SRC_COLOR)

			glTexEnvi(GL_TEXTURE_ENV, GL_COMBINE_ALPHA, GL_MODULATE)
			glTexEnvi(GL_TEXTURE_ENV, GL_SOURCE0_ALPHA, GL_PRIMARY_COLOR)
			glTexEnvi(GL_TEXTURE_ENV, GL_OPERAND0_ALPHA, GL_SRC_ALPHA)
			glTexEnvi(GL_TEXTURE_ENV, GL_SOURCE1_ALPHA, GL_TEXTURE)
			glTexEnvi(GL_TEXTURE_ENV, GL_OPERAND1_ALPHA, GL_SRC_ALPHA)

			initBlend(group)
			drawRect(pose, x, y, u, v, w, h)
		}
	}

	private fun initBlend(group: CreativeModeTab) {
		val h = (group.hashCode()%360)/360f*TAU
		val r = Mth.sin(h)
		val g = Mth.sin(h+TAU/3)
		val b = Mth.sin(h-TAU/3)
		glEnable(GL_BLEND)
		glBlendFunc(GL_SRC_ALPHA, GL_ONE)
		glBlendEquation(GL_FUNC_REVERSE_SUBTRACT)
		glColor4f(1-r, 1-g, 1-b, 0.25f)
	}
}
