package kawaru.chikei

import java.util.Optional

import mc.client.gui.screens.worldselection.WorldPreset
import mc.core.Registry
import mc.server.level.ServerLevel
import mc.world.level.chunk.ChunkAccess
import mc.world.level.chunk.ProtoChunk
import mc.world.level.chunk.LevelChunk
import mc.world.level.levelgen.Heightmap

object Asm {
	@JvmStatic
	fun addPreset(presets: List<WorldPreset>) = presets + Chikei.BANQUET

	@JvmStatic
	fun addEditor(editors: Map<Optional<WorldPreset>, WorldPreset.PresetEditor>) = editors

	@JvmStatic
	fun registerGenerator() {
		Registry.register(Registry.CHUNK_GENERATOR, "kawaru-chikei:banquet", BanquetChunkGenerator.CODEC);
	}

	@JvmStatic
	fun primeHeightmap(maps: Set<Heightmap.Types>, level: ServerLevel, chunk: ChunkAccess) {
		val gen = level.chunkSource.generator as? BanquetChunkGenerator ?: return
		if(gen.parent.settings.get().bedrockRoofPosition != -10 && !level.dimensionType().hasCeiling())
			CaveHeightmap.primeHeightmap(chunk, maps)
	}

	@JvmStatic
	fun createHeightmap(chunk: ChunkAccess, type: Heightmap.Types): Heightmap? {
		// println("$chunk $type")
		// return Heightmap(chunk, type)
		// println("$chunk $type")
		// val lchunk = chunk as? LevelChunk ?: return null
		// val level = lchunk.level as? ServerLevel ?: return null
		// level.chunkSource.generator as? BanquetChunkGenerator ?: return null
		return CaveHeightmap(chunk, type)
	}
}
