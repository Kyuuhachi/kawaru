package kawaru.chikei

import com.mojang.serialization.Codec
import com.mojang.serialization.codecs.RecordCodecBuilder

import mc.core.BlockPos
import mc.core.Registry
import mc.core.RegistryAccess
import mc.server.level.ServerLevel
import mc.server.level.WorldGenRegion
import mc.world.entity.MobCategory
import mc.world.level.ChunkPos
import mc.world.level.LevelAccessor
import mc.world.level.StructureFeatureManager
import mc.world.level.WorldGenLevel
import mc.world.level.biome.Biome
import mc.world.level.biome.BiomeManager
import mc.world.level.chunk.ChunkAccess
import mc.world.level.chunk.ChunkGenerator
import mc.world.level.levelgen.GenerationStep
import mc.world.level.levelgen.Heightmap
import mc.world.level.levelgen.NoiseBasedChunkGenerator
import mc.world.level.levelgen.feature.StructureFeature
import mc.world.level.levelgen.structure.templatesystem.StructureManager

// I don't quite like this implementation, but I need some way to communicate to
// the heightmap stuff that we're up to something
data class BanquetChunkGenerator(val parent: NoiseBasedChunkGenerator) : ChunkGenerator(null, null) {
	companion object {
		val CODEC: Codec<BanquetChunkGenerator> = RecordCodecBuilder.create {
			it.group(
				NoiseBasedChunkGenerator.CODEC.fieldOf("parent").forGetter{it.parent}
			).apply(it, ::BanquetChunkGenerator)
		}
	}
	override protected fun codec() = CODEC

	override fun withSeed(seed: Long) = copy(parent=parent.withSeed(seed) as NoiseBasedChunkGenerator);

	override fun createBiomes(biomes: Registry<Biome>, chunk: ChunkAccess) = parent.createBiomes(biomes, chunk)
	override fun applyCarvers(l: Long, biomes: BiomeManager, chunk: ChunkAccess, carving: GenerationStep.Carving) = parent.applyCarvers(l, biomes, chunk, carving)
	override fun findNearestMapFeature(level: ServerLevel, struct: StructureFeature<*>, pos: BlockPos, i: Int, b: Boolean) = parent.findNearestMapFeature(level, struct, pos, i, b)

	override fun applyBiomeDecoration(region: WorldGenRegion, feat: StructureFeatureManager) = parent.applyBiomeDecoration(region, feat)
	override fun buildSurfaceAndBedrock(region: WorldGenRegion, chunk: ChunkAccess) = parent.buildSurfaceAndBedrock(region, chunk)
	override fun spawnOriginalMobs(region: WorldGenRegion) = parent.spawnOriginalMobs(region)

	override fun getSettings() = parent.getSettings()
	override fun getSpawnHeight() = parent.getSpawnHeight()
	override fun getBiomeSource() = parent.getBiomeSource()
	override fun getGenDepth() = parent.getGenDepth()
	override fun getSeaLevel() = parent.getSeaLevel()

	override fun getMobsAt(biome: Biome, feat: StructureFeatureManager, cat: MobCategory, pos: BlockPos) = parent.getMobsAt(biome, feat, cat, pos)
	override fun createStructures(reg: RegistryAccess, feat: StructureFeatureManager, chunk: ChunkAccess, struct: StructureManager, l: Long) = parent.createStructures(reg, feat, chunk, struct, l)
	override fun createReferences(level: WorldGenLevel, feat: StructureFeatureManager, chunk: ChunkAccess) = parent.createReferences(level, feat, chunk)
	override fun fillFromNoise(level: LevelAccessor, feat: StructureFeatureManager, chunk: ChunkAccess) = parent.fillFromNoise(level, feat, chunk)

	// Wish I could override these, but it's better to mess with the heightmaps directly
	override fun getBaseHeight(x: Int, z: Int, type: Heightmap.Types) = parent.getBaseHeight(x, z, type)
	override fun getBaseColumn(x: Int, z: Int) = parent.getBaseColumn(x, z)
	override fun getFirstFreeHeight(x: Int, z: Int, type: Heightmap.Types) = parent.getFirstFreeHeight(x, z, type)
	override fun getFirstOccupiedHeight(x: Int, z: Int, type: Heightmap.Types) = parent.getFirstOccupiedHeight(x, z, type)
	override fun hasStronghold(cpos: ChunkPos) = parent.hasStronghold(cpos)
}
