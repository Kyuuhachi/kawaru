package kawaru.chikei

import mc.core.BlockPos
import mc.world.level.block.state.BlockState
import mc.world.level.chunk.ChunkAccess
import mc.world.level.levelgen.Heightmap
import mc.world.level.levelgen.NoiseBasedChunkGenerator

class CaveHeightmap(val chunk: ChunkAccess, type: Heightmap.Types) : Heightmap(chunk, type) {
	companion object {
		fun primeHeightmap(chunk: ChunkAccess, maps: Set<Heightmap.Types>) {
			enumValues<Heightmap.Types>().forEach { type ->
				val heightmap = chunk.getOrCreateHeightmapUnprimed(type)
				for(x in 0 until 16)
					for(z in 0 until 16)
						updateHeightmap(heightmap, x, z)
			}
		}

		fun updateHeightmap(heightmap: Heightmap, x: Int, z: Int) {
			val top = heightmap.chunk.getHighestSectionPosition() + 16
			val pos = BlockPos.MutableBlockPos()
			var stage = 0
			for(y in top-1 downTo 0) {
				pos.set(x, y, z)
				val block = heightmap.chunk.getBlockState(pos)
				when(stage) {
					0 -> if( heightmap.isOpaque.test(block)) { heightmap.setHeight(x, z, y+1); stage++ }
					1 -> if(!heightmap.isOpaque.test(block)) { stage++ }
					2 -> if( heightmap.isOpaque.test(block)) { heightmap.setHeight(x, z, y+1); break }
				}
			}
		}
	}

	override fun update(x: Int, new: Int, z: Int, block: BlockState): Boolean {
		val prev = getFirstAvailable(x, z)
		updateHeightmap(this, x, z)
		return prev != getFirstAvailable(x, z)
	}
}
