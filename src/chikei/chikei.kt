package kawaru.chikei

import mc.client.gui.screens.worldselection.WorldPreset
import mc.core.Registry
import mc.world.level.biome.Biome
import mc.world.level.biome.OverworldBiomeSource
import mc.world.level.block.Blocks;
import mc.world.level.block.state.BlockState;
import mc.world.level.levelgen.NoiseBasedChunkGenerator
import mc.world.level.levelgen.NoiseGeneratorSettings
import mc.world.level.levelgen.NoiseSamplingSettings
import mc.world.level.levelgen.NoiseSettings
import mc.world.level.levelgen.NoiseSlideSettings
import mc.world.level.levelgen.StructureSettings

import kawaru.kokoro.util.text.*

object Chikei {
	val BANQUET = object : WorldPreset(null) {
		override fun description() = 𝕋("kawaru-chikei:generator.banquet")

		override protected fun generator(biomes: Registry<Biome>, noises: Registry<NoiseGeneratorSettings>, seed: Long)
			= BanquetChunkGenerator(NoiseBasedChunkGenerator(
				OverworldBiomeSource(seed, false, false, biomes),
				seed,
				{ caves(StructureSettings(true), Blocks.GLASS.defaultBlockState(), Blocks.BLUE_STAINED_GLASS.defaultBlockState()) },
			))
	}

	val SURFACE_SETTINGS = NoiseSettings(
		256, // height
		NoiseSamplingSettings(0.9999999814507745, 0.9999999814507745, 80.0, 160.0), // noise sampling
		NoiseSlideSettings(-10, 3, 0), NoiseSlideSettings(-30, 0, 0), // top/bottom slide
		1, 2, // noise slide horizontal/vertical
		1.0, -0.46875, // density factor/offset
		true, // useSimplexSurfaceNoise
		true, // randomDensityOffset
		false, // islandNoiseOverride
		false, // isAmplified
	)

	val CAVES_SETTINGS = NoiseSettings(
		128, // height
		NoiseSamplingSettings(1.0, 3.0, 80.0, 60.0), // noise sampling
		NoiseSlideSettings(120, 3, 0), NoiseSlideSettings(320, 4, -1),  // top/bottom slide
		1, 2, // noise slide horizontal/vertical
		0.0, 0.019921875, // density factor/offset
		false, // useSimplexSurfaceNoise
		false, // randomDensityOffset
		false, // islandNoiseOverride
		false, // isAmplified
	)

	val ISLANDS_SETTINGS = NoiseSettings(
		128, // height
		NoiseSamplingSettings(2.0, 1.0, 80.0, 160.0), // noise sampling
		NoiseSlideSettings(-3000, 64, -46), NoiseSlideSettings(-30, 7, 1),  // top/bottom slide
		2, 1, // noise slide horizontal/vertical
		0.0, 0.0, // density factor/offset
		true, // useSimplexSurfaceNoise
		false, // randomDensityOffset
		false, // islandNoiseOverride
		false, // isAmplified
	)

	fun surface(struct: StructureSettings, block: BlockState, fluid: BlockState) = NoiseGeneratorSettings(
		struct, SURFACE_SETTINGS, block, fluid,
		-10, 0, 63, false, // floor, roof, sea, disableMobs
	)

	fun caves(struct: StructureSettings, block: BlockState, fluid: BlockState) = NoiseGeneratorSettings(
		struct, CAVES_SETTINGS, block, fluid,
		0, 0, 32, false, // floor, roof, sea, disableMobs
	)

	fun islands(struct: StructureSettings, block: BlockState, fluid: BlockState) = NoiseGeneratorSettings(
		struct, ISLANDS_SETTINGS, block, fluid,
		-10, -10, 0, false, // floor, roof, sea, disableMobs
	)

	// For The End, set islandNoiseOverride and disableMobs to true
}
