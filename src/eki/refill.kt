package kawaru.eki

import mc.client.player.LocalPlayer
import mc.world.InteractionHand
import mc.world.entity.LivingEntity
import mc.world.inventory.AbstractContainerMenu as Menu
import mc.world.inventory.ClickType
import mc.world.item.ItemStack

import kawaru.kokoro.util.*

object Refill {
	lateinit var preItems: Map<Int, ItemStack>
	var LocalPlayer.toRefill by extraField { mutableListOf<Pair<ItemStack, Int>>() }

	// Breakage is handled by the server, not client. For some reason handling
	// the refilling immediately causes desynchronization, so we gotta delay it.
	fun onBreak(entity: LivingEntity, item: ItemStack) {
		if(entity.level.isClientSide && entity === PLAYER)
			for(slot in listOf(PLAYER.inventory.selected, 36, 37, 38, 39, 40))
				if(item === PLAYER.inventory.getItem(slot))
					PLAYER.toRefill.add(item to slot)
	}

	fun preTick() {
		if(MC.player == null) return
		preItems = InteractionHand.values().associate {
			val slot = when(it) {
				InteractionHand.MAIN_HAND -> PLAYER.inventory.selected
				InteractionHand.OFF_HAND -> 40
			}
			slot to PLAYER.inventory.getItem(slot).copy()
		}
	}

	fun postTick() {
		if(MC.player == null) return

		PLAYER.toRefill.forEach{(a,b) -> doRefill(a, b)}
		PLAYER.toRefill.clear()

		for((slot, preItem) in preItems.entries)
			if(!preItem.isEmpty && PLAYER.inventory.getItem(slot).isEmpty)
				doRefill(preItem, slot)
	}

	fun doRefill(item: ItemStack, targetSlot: Int) {
		val sourceSlot =
			PLAYER.containerMenu.slots
			.filter { it.container == PLAYER.inventory }
			.filter { it.slot < 36 && it.slot != PLAYER.inventory.selected }
			.filter { Menu.consideredTheSameItem(
				it.item.copy().apply{damageValue=0},
				item.copy().apply{damageValue=0},
			) }
			.firstOrNull()
			?.index
			?: return

		MC.gameMode!!.handleInventoryMouseClick(
			PLAYER.containerMenu.containerId,
			sourceSlot,
			targetSlot,
			ClickType.SWAP,
			PLAYER,
		)
	}
}
