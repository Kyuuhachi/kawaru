package kawaru.eki

import com.mojang.blaze3d.vertex.PoseStack
import mc.client.KeyMapping
import mc.client.gui.Gui
import mc.client.multiplayer.MultiPlayerGameMode as GameMode
import mc.client.player.LocalPlayer
import mc.core.BlockPos
import mc.core.Direction
import mc.world.InteractionHand
import mc.world.entity.player.Inventory
import mc.world.inventory.ClickType
import mc.world.inventory.CraftingContainer
import mc.world.phys.BlockHitResult

import kawaru.kokoro.util.*
import kawaru.kokoro.util.text.*
import kawaru.kokoro.util.draw.*

abstract class LockableKeyMapping(name: String, dflt: Int, cat: String) : KeyMapping(name, dflt, cat) {
	private var lastDownTime = -1000L
	private var wasDown = false
	abstract var isLocked: Boolean
	override fun setDown(down: Boolean) {
		super.setDown(down)
		val now = System.currentTimeMillis()
		if(down && !wasDown)
			if(isLocked)
				isLocked = false
			else if(now - lastDownTime < 250)
				isLocked = true
			else
				lastDownTime = now
		wasDown = down
	}

	override fun isDown() = super.isDown() || isLocked
}

val KEY_UNDERFOOT = KeyMapping("kawaru-eki:key.underfoot", 'J'.𝒾, "key.categories.misc")
val KEY_REPLACE = object: LockableKeyMapping("kawaru-eki:key.replace", 'K'.𝒾, "key.categories.misc") {
	private var GameMode._replaceMode: Boolean by extraField { false }
	override var isLocked
		get() = MC.gameMode!!._replaceMode
		set(v) {
			MC.gameMode!!._replaceMode = v
			if(v)
				PLAYER.displayClientMessage(𝕋("kawaru-eki:replaceMode.on"), true);
			else
				PLAYER.displayClientMessage(𝕋("kawaru-eki:replaceMode.off"), true);
		}
}
val KEY_ALIGN = KeyMapping("kawaru-eki:key.align", 346 /*GLFW_KEY_RIGHT_ALT*/, "key.categories.misc")

val KEY_HOTBAR_RIGHT = KeyMapping("kawaru-eki:key.hotbar.right", 262 /*GLFW_KEY_RIGHT*/, "key.categories.inventory")
val KEY_HOTBAR_LEFT  = KeyMapping("kawaru-eki:key.hotbar.left",  263 /*GLFW_KEY_LEFT*/,  "key.categories.inventory")
val KEY_HOTBAR_DOWN  = KeyMapping("kawaru-eki:key.hotbar.down",  264 /*GLFW_KEY_DOWN*/,  "key.categories.inventory")
val KEY_HOTBAR_UP    = KeyMapping("kawaru-eki:key.hotbar.up",    265 /*GLFW_KEY_UP*/,    "key.categories.inventory")

val KEYS = arrayOf<KeyMapping>(
	KEY_UNDERFOOT, KEY_REPLACE, KEY_ALIGN,
	KEY_HOTBAR_RIGHT, KEY_HOTBAR_LEFT, KEY_HOTBAR_DOWN, KEY_HOTBAR_UP,
)

object Eki {
	private val mode get() = MC.gameMode!!

	fun LocalPlayer.handleKeys() {
		if(KEY_UNDERFOOT.isDown)
			mode.tryPlace(BlockHitResult(PLAYER.position(), Direction.UP, PLAYER.blockPosition().below(), false))

		while(KEY_ALIGN.consumeClick()) {
			xRot = Math.round(xRot/45)*45f % 360f
			yRot = Math.round(yRot/45)*45f % 360f
			setPos(
				Math.round(position().x/0.5)*0.5,
				position().y,
				Math.round(position().z/0.5)*0.5,
			)
		}

		while(KEY_HOTBAR_RIGHT.consumeClick()) inventory.swapPaint(-1.0)
		while(KEY_HOTBAR_LEFT.consumeClick())  inventory.swapPaint(1.0)
		while(KEY_HOTBAR_UP.consumeClick())    scrollInventory(inventory.selected, +1)
		while(KEY_HOTBAR_DOWN.consumeClick())  scrollInventory(inventory.selected, -1)
	}

	fun Inventory.getColumn(column: Int, dir: Int) =
		(1 until 4)
			.map { (column + it*dir*9).mod(36) }
			.filter { !items[it].isEmpty }

	fun LocalPlayer.scrollInventory(column: Int, dir: Int) {
		var changed = false

		val slots =
			if(inventory.items[column].isEmpty && dir > 0)
				inventory.getColumn(column, -dir).take(1)
			else
				inventory.getColumn(column, dir)

		for(slot in slots) {
			val idx = containerMenu.slots.indexOfFirst { it.container === inventory && it.slot == slot }
			if(idx == -1) continue
			mode.handleInventoryMouseClick(containerMenu.containerId, idx, column, ClickType.SWAP, this)
			changed = true
		}

		if(changed)
			inventory.items[column].popTime = 5
	}

	fun renderHotbar(pose: PoseStack, i: Int, x: Int, y: Int) {
		val inv = PLAYER.inventory
		// if(i != inv.selected) continue
		val col = inv.getColumn(i, -1)
		if(!col.isEmpty())
			pose {
				pose.translate(x, y)
				pose.scale(0.5, 0.5)
				pose.realize {
					MC.itemRenderer.blitOffset -= 50
					MC.itemRenderer.renderGuiItem(inv.items[col.first()], -4, -4)
					MC.itemRenderer.blitOffset += 50
					if(col.size > 1) {
						MC.itemRenderer.blitOffset += 50
						MC.itemRenderer.renderGuiItem(inv.items[col.last()], -4, 20)
						MC.itemRenderer.blitOffset -= 50
					}
				}
			}
	}

	fun GameMode.afterBreak(pos: BlockPos) {
		if(KEY_REPLACE.isDown)
			tryPlace(BlockHitResult(PLAYER.position(), Direction.UP, pos, false))
	}

	private fun GameMode.tryPlace(hit: BlockHitResult): Boolean {
		require(MC.isSameThread)
		for(hand in InteractionHand.values()) {
			val item = PLAYER.getItemInHand(hand)
			if(item.isEmpty || PLAYER.cooldowns.isOnCooldown(item.item)) continue

			if(useItemOn(PLAYER, LEVEL, hand, hit).consumesAction())
				return true
		}
		return false
	}

	private var Inventory.lastSelected by extraField { -1 }
	fun Inventory.setSelected(value: Int) {
		lastSelected = selected
		.also {
			if(lastSelected != -1 && selected == value)
				selected = lastSelected
			else
				selected = value
		}
	}
}
