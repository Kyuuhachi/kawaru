package kawaru.kokoro.config

import java.io.File
import java.nio.file.Files

import org.yaml.snakeyaml.Yaml
import org.yaml.snakeyaml.error.Mark
import org.yaml.snakeyaml.nodes.*
import org.yaml.snakeyaml.resolver.Resolver

import kawaru.kokoro.internal.Kokoro

private fun Node.parse() : Config {
	val tag = if(tag.startsWith(Tag.PREFIX)) "!!" + tag.value.substring(Tag.PREFIX.length) else tag.value
	val explicit = @Suppress("DEPRECATION") !isResolved
	return when(this) {
		is ScalarNode -> Scalar(tag, value, explicit)
		is SequenceNode -> Sequence(tag, value.map{it.parse()}, explicit)
		is MappingNode -> Mapping(tag, value.map{it.keyNode.parse() to it.valueNode.parse()}, explicit)
		else -> throw IllegalArgumentException()
	}
}

sealed class Config(val tag : String, val explicit : Boolean) {
	protected val tagStr get() = if(explicit) "$tag " else ""

	open val asScalar get() : Scalar = throw IllegalArgumentException("Expected scalar")
	open val asMapping get() : Mapping = throw IllegalArgumentException("Expected mapping")
	open val asSequence get() : Sequence = throw IllegalArgumentException("Expected sequence")

	companion object {
		operator fun invoke(name : String) : Config {
			val conffile = File("config/$name.yml")
			if(!conffile.exists()) {
				conffile.parentFile.mkdirs()
				Files.newInputStream(Kokoro.mods[name]!!.path.resolve("config.yml.default")).copyTo(conffile.outputStream())
			}
			val yaml = Yaml().apply{ addImplicitResolver(Tag(Tag.PREFIX+"value"), Resolver.VALUE, "=") }
			return yaml.compose(conffile.reader()).parse()
		}
	}
}

class Scalar(tag : String, val value : String, explicit : Boolean)
	: Config(tag, explicit) {
	override fun toString() = "$tagStr\"$value\""
	override val asScalar = this
}

class Mapping(tag : String, private val value : List<Pair<Config,Config>>, explicit : Boolean)
	: Config(tag, explicit), List<Pair<Config, Config>> by value {
	override fun toString() = joinToString(", ", "$tagStr{", "}") {"${it.first}: ${it.second}"}
	override val asMapping = this
	override val asScalar by lazy { single{it.first.tag == "!!value"}.second.asScalar }
}

class Sequence(tag : String, private val value : List<Config>, explicit : Boolean)
	: Config(tag, explicit), List<Config> by value {
	override fun toString() = joinToString(", ", "$tagStr[", "]") {"${it}"}
	override val asSequence = this
}

val Config.isScalar get() = runCatching { asScalar }.isSuccess
val Config.isMapping get() = runCatching { asMapping }.isSuccess
val Config.isSequence get() = runCatching { asSequence }.isSuccess

fun Config.asMap(value : Boolean = false) : Map<String, Config> =
	mutableMapOf<String, Config>().apply {
		for((k, v) in asMapping) {
			require(k is Scalar, {"Expected scalar"})
			require(k.asScalar.value !in this, {"Duplicate key"})
			if(!value || k.tag != "!!value") {
				require(!k.explicit, {"Unexpected tag"})
				set(k.asScalar.value, v)
			}
		}
	}

fun Config.asBool() = run {
	require(asScalar.tag == "!!bool", {"Must be !!bool"})
	when(asScalar.value.toLowerCase()) {
		"y", "yes", "true", "on" -> true
		"n", "no", "false", "off" -> false
		else -> throw IllegalArgumentException("illegal !!bool")
	}
}

inline fun <reified T : Enum<T>> Config.asEnum() =
	enumValues<T>().singleOrNull{it.name.equals(asScalar.value, ignoreCase=true)}
	?: throw IllegalArgumentException("Unknown enum constant")
