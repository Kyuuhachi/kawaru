package kawaru.kokoro.util

import java.text.DecimalFormat
import java.text.DecimalFormatSymbols
import java.util.Locale
import java.util.WeakHashMap
import kotlin.reflect.KProperty

import mc.client.Minecraft
import mc.core.BlockPos
import mc.core.Vec3i
import mc.util.Mth;
import mc.world.level.block.state.BlockState
import mc.world.level.chunk.ChunkAccess
import mc.world.phys.Vec3

import com.mojang.datafixers.util.Pair as MPair

inline val MC get() = Minecraft.getInstance()
inline val PLAYER get() = MC.player ?: throw NullPointerException("PLAYER")
inline val LEVEL get() = MC.level ?: throw NullPointerException("LEVEL")

fun Double.toString(x : String) = DecimalFormat(x, DecimalFormatSymbols.getInstance(Locale.ROOT)).format(this)
fun Float.toString(x : String) = DecimalFormat(x, DecimalFormatSymbols.getInstance(Locale.ROOT)).format(this)
inline val Char.𝒾 get() = toInt()
inline val Int.𝒻 get() = toFloat()
inline val Int.𝒹 get() = toDouble()
inline val Int.𝓁 get() = toLong()
inline val Long.𝒾 get() = toInt()
inline val Float.𝒹 get() = toDouble()
inline val Float.𝒾 get() = Mth.floor(this)
inline val Double.𝒻 get() = toFloat()
inline val Double.𝒾 get() = Mth.floor(this)
inline val Double.𝓁 get() = Mth.floor(this)

fun Int.mod(b: Int) = ((this % b) + b) % b
fun Long.mod(b: Long) = ((this % b) + b) % b

operator fun <A, B> MPair<A, B>.component1() = first
operator fun <A, B> MPair<A, B>.component2() = second
operator fun Vec3.component1() = x
operator fun Vec3.component2() = y
operator fun Vec3.component3() = z
operator fun Vec3i.component1() = x
operator fun Vec3i.component2() = y
operator fun Vec3i.component3() = z

inline fun profile(section : String, thunk : () -> Unit) {
	MC.profiler.push(section)
	thunk()
	MC.profiler.pop()
}

@Deprecated("")
private val _pos = BlockPos.MutableBlockPos()
@Deprecated("")
fun pos(x : Int, y : Int, z : Int) = _pos.set(x,y,z)
@Deprecated("")
operator fun ChunkAccess.set(x : Int, y : Int, z : Int, block : BlockState) = setBlockState(pos(x,y,z), block, false)
@Deprecated("")
operator fun ChunkAccess.get(x : Int, y : Int, z : Int) = getBlockState(pos(x,y,z))

class extraField<K, V>(private val default: K.()->V) {
	private val map = WeakHashMap<K, V>()
	operator fun getValue(self: K, prop: KProperty<*>) = this.map.getOrPut(self, {default(self)})
	operator fun setValue(self: K, prop: KProperty<*>, value: V) { this.map.put(self, value) }
}
