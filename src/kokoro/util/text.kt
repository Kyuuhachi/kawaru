package kawaru.kokoro.util.text

import mc.network.chat.Style
import mc.network.chat.TextComponent
import mc.network.chat.MutableComponent
import mc.network.chat.TranslatableComponent

import mc.network.chat.TextColor
import mc.ChatFormatting

typealias Component = mc.network.chat.Component


data class StyleDsl(var style : Style) {
	inline var color         get() = this.style.getColor();       set (v) { this.style = style.withColor(v) }
	inline var bold          get() = this.style.isBold();         set (v) { this.style = style.withBold(v) }
	inline var italic        get() = this.style.isItalic();       set (v) { this.style = style.withItalic(v) }
	inline var underlined    get() = this.style.isUnderlined();   set (v) { this.style = style.withUnderlined(v) }
	inline val strikethrough get() = this.style.isStrikethrough() // set(v) { this.style = style.withStrikethrough(v) }
	inline val obfuscated    get() = this.style.isObfuscated()    // set(v) { this.style = style.withObfuscated(v) }
	inline var clickEvent    get() = this.style.getClickEvent();  set (v) { this.style = style.withClickEvent(v) }
	inline var hoverEvent    get() = this.style.getHoverEvent();  set (v) { this.style = style.withHoverEvent(v) }
	inline var insertion     get() = this.style.getInsertion();   set (v) { this.style = style.withInsertion(v) }
	inline var font          get() = this.style.getFont();        set (v) { this.style = style.withFont(v) }

	val BLACK        = TextColor.fromLegacyFormat(ChatFormatting.BLACK)!!
	val DARK_BLUE    = TextColor.fromLegacyFormat(ChatFormatting.DARK_BLUE)!!
	val DARK_GREEN   = TextColor.fromLegacyFormat(ChatFormatting.DARK_GREEN)!!
	val DARK_AQUA    = TextColor.fromLegacyFormat(ChatFormatting.DARK_AQUA)!!
	val DARK_RED     = TextColor.fromLegacyFormat(ChatFormatting.DARK_RED)!!
	val DARK_PURPLE  = TextColor.fromLegacyFormat(ChatFormatting.DARK_PURPLE)!!
	val GOLD         = TextColor.fromLegacyFormat(ChatFormatting.GOLD)!!
	val GRAY         = TextColor.fromLegacyFormat(ChatFormatting.GRAY)!!
	val DARK_GRAY    = TextColor.fromLegacyFormat(ChatFormatting.DARK_GRAY)!!
	val BLUE         = TextColor.fromLegacyFormat(ChatFormatting.BLUE)!!
	val GREEN        = TextColor.fromLegacyFormat(ChatFormatting.GREEN)!!
	val AQUA         = TextColor.fromLegacyFormat(ChatFormatting.AQUA)!!
	val RED          = TextColor.fromLegacyFormat(ChatFormatting.RED)!!
	val LIGHT_PURPLE = TextColor.fromLegacyFormat(ChatFormatting.LIGHT_PURPLE)!!
	val YELLOW       = TextColor.fromLegacyFormat(ChatFormatting.YELLOW)!!
	val WHITE        = TextColor.fromLegacyFormat(ChatFormatting.WHITE)!!

	fun merge(x: ChatFormatting) { this.style = this.style.applyFormat(x) }
}

fun 𝕊(x : Any) : MutableComponent = TextComponent(x.toString())
fun 𝕊(x : Any, f : StyleDsl.() -> Unit) = 𝕊(x).invoke(f)
fun 𝕋(x : String, vararg args : Any) : MutableComponent = TranslatableComponent(x, *args)
fun 𝕋(x : String, vararg args : Any, f : StyleDsl.() -> Unit) = 𝕋(x, *args).invoke(f)
operator fun MutableComponent.invoke(f : StyleDsl.() -> Unit) = withStyle { StyleDsl(it).apply(f).style }
val ℕ = TextComponent("")
