package kawaru.kokoro.util.draw

import kawaru.kokoro.util.*

import java.lang.reflect.Array as RArray
import java.lang.reflect.Field as RField
import java.lang.reflect.Modifier as RModifier

import com.mojang.blaze3d.platform.GlStateManager
import com.mojang.blaze3d.systems.RenderSystem
import com.mojang.blaze3d.vertex.BufferBuilder
import com.mojang.blaze3d.vertex.DefaultVertexFormat
import com.mojang.blaze3d.vertex.Tesselator
import com.mojang.blaze3d.vertex.VertexConsumer
import com.mojang.blaze3d.vertex.VertexFormat
import com.mojang.math.Matrix4f
import com.mojang.math.Transformation
import mc.client.gui.Font
import mc.client.renderer.MultiBufferSource
import mc.client.renderer.RenderStateShard
import mc.network.chat.Component
import mc.resources.ResourceLocation
import mc.world.phys.Vec3

import org.lwjgl.opengl.GL11
import org.lwjgl.opengl.GL14

typealias PoseStack = com.mojang.blaze3d.vertex.PoseStack

val PI = Math.PI.𝒻
val TAU = 2 * PI

data class RGBA(val r: Float, val g: Float, val b: Float, val a: Float=1f)
fun RGBA(v: Vec3) = RGBA(v.x.𝒻, v.y.𝒻, v.z.𝒻)
val WHITE = RGBA(1f, 1f, 1f)
val BLACK = RGBA(0f, 0f, 0f)
operator fun RGBA.times(a: Float) = copy(a=this.a*a)
operator fun RGBA.div(a: Float) = copy(a=this.a/a)
val Int.rgb get() = rgba.copy(a=1f)
val Int.rgba get() = RGBA(
	(this ushr 16 and 0xFF).𝒻 / 255,
	(this ushr  8 and 0xFF).𝒻 / 255,
	(this ushr  0 and 0xFF).𝒻 / 255,
	(this ushr 24 and 0xFF).𝒻 / 255)
val RGBA.𝒾 get() =
	( ((r.coerceIn(0f, 1f) * 255).𝒾 shl 16)
	+ ((g.coerceIn(0f, 1f) * 255).𝒾 shl  8)
	+ ((b.coerceIn(0f, 1f) * 255).𝒾 shl  0)
	+ ((a.coerceIn(0f, 1f) * 255).𝒾 shl 24) )

inline val FONT get() = MC.font

// I only want to call bidiReorder and stuff once
private inline fun Font.proc(
	pose: PoseStack,
	i: Component,
	x: Int,
	y: Int,
	xalign: Float,
	yalign: Float,
	body: ((Int, Int, RGBA, Boolean) -> Unit) -> Unit
) {
	val s = i.visualOrderText
	val x0 = if(xalign != 0f) x - ((width(s)+1) * xalign).𝒾 else x
	val y0 = y - (lineHeight * yalign).𝒾
	val source = MultiBufferSource.immediate(Tesselator.getInstance().builder)
	val mat = pose.last().pose()
	body({ xo, yo, c, sh -> renderText(s, x0.𝒻+xo, y0.𝒻+yo, c.𝒾, sh, mat, source, false, 0, 0x00F000F0) })
	source.endBatch()
}

operator fun Font.invoke(pose: PoseStack, i: Component, x0: Int, y0: Int, c: RGBA, xalign: Float=0f, yalign: Float=0f) =
	proc(pose, i, x0, y0, xalign, yalign) {
		it(0, 0, c, false)
	}
fun Font.shadow(pose: PoseStack, i: Component, x0: Int, y0: Int, c: RGBA, xalign: Float=0f, yalign: Float=0f) =
	proc(pose, i, x0, y0, xalign, yalign) {
		it(0, 0, c, true)
		it(0, 0, c, false)
	}
fun Font.outline(pose: PoseStack, i: Component, x0: Int, y0: Int, c: RGBA, xalign: Float=0f, yalign: Float=0f) =
	outlineCustom(pose, i, x0, y0, c, c, xalign, yalign)
fun Font.outlineCustom(pose: PoseStack, i: Component, x0: Int, y0: Int, c1: RGBA, c2: RGBA, xalign: Float=0f, yalign: Float=0f) =
	proc(pose, i, x0, y0, xalign, yalign) {
		it(-1,  0, c2, false)
		it(+1,  0, c2, false)
		it( 0, -1, c2, false)
		it( 0, +1, c2, false)
		it( 0,  0, c1, false)
	}

fun drawRect(pose: PoseStack, x: Int, y: Int, u: Int, v: Int, w: Int, h: Int, tw: Int=256, th: Int=256, z: Float=0f) {
	drawRect(pose, x.𝒻, y.𝒻, u.𝒻, v.𝒻, w.𝒻, h.𝒻, tw.𝒻, th.𝒻, z)
}

data class VertexCreator(val pose: PoseStack, val buf: VertexConsumer) {
	fun vertex(x: Float,  y: Float, z: Float)   = buf.vertex(pose.last().pose(), x, y, z)
	fun vertex(x: Double, y: Double, z: Double) = buf.vertex(pose.last().pose(), x.𝒻, y.𝒻, z.𝒻)
	fun vertex(x: Int,    y: Int)               = buf.vertex(pose.last().pose(), x.𝒻, y.𝒻, 0f)

	fun VertexConsumer.color(r: Float, g: Float, b: Float) = color(r,g,b,1f)
	fun VertexConsumer.color(rgba: RGBA) = color(rgba.r, rgba.g, rgba.b, rgba.a)
	inline val VertexConsumer.end get() = endVertex()
}

fun drawRect(pose: PoseStack, x: Float, y: Float, u: Float, v: Float, w: Float, h: Float, tw: Float=256f, th: Float=256f, z: Float=0f) {
	tessellate(pose, DefaultVertexFormat.POSITION_TEX) {
		vertex(x+0, y+h, z).uv((u+0)/tw, (v+h)/th).end
		vertex(x+w, y+h, z).uv((u+w)/tw, (v+h)/th).end
		vertex(x+w, y+0, z).uv((u+w)/tw, (v+0)/th).end
		vertex(x+0, y+0, z).uv((u+0)/tw, (v+0)/th).end
	}
}

inline fun tessellate(
	pose: PoseStack,
	format: VertexFormat,
	shape: Int = GL.QUADS,
	thunk: VertexCreator.() -> Unit
) {
	val t = Tesselator.getInstance()
	t.builder.begin(shape, format)
	thunk(VertexCreator(pose, t.builder))
	t.end()
}

// TODO use VarHandle instead
private fun getPrimitives(o: Any?, cls: Class<*>) : List<Pair<Any?, RField>> =
	if(cls.isArray) {
		(0 until RArray.getLength(o)).flatMap { getPrimitives(RArray.get(o, it), cls.componentType) }
	} else cls.declaredFields.flatMap {
		it.isAccessible = true
		when {
			it.type.isPrimitive || it.type.isEnum -> if(it.modifiers and RModifier.FINAL != 0) listOf() else listOf(o to it)
			it.type == java.nio.FloatBuffer::class.java -> listOf()
			it.type == com.mojang.math.Vector3f::class.java -> listOf()
			else -> getPrimitives(it[o], it.type)
		}
	}

private val glsmFields = getPrimitives(null, GlStateManager::class.java)

fun saveGlsm() : Any = glsmFields.map { fd -> fd.second[fd.first] }
fun loadGlsm(fields: Any) = glsmFields.zip(fields as List<Any?>).forEach { (fd, v) -> fd.second[fd.first] = v }

@Deprecated("Interacts poorly with the rest of the system, and slow")
inline fun glPushAttrib(thunk: () -> Unit) {
	val fields = saveGlsm()
	GL11.glPushAttrib(-1)
	thunk()
	GL11.glPopAttrib()
	loadGlsm(fields)
}

inline operator fun PoseStack.invoke(thunk: () -> Unit) {
	this.pushPose()
	thunk()
	this.popPose()
}

fun PoseStack.translate(x: Int, y: Int) = translate(x, y, 0)
fun PoseStack.translate(x: Int, y: Int, z: Int) = translate(x.𝒹, y.𝒹, z.𝒹)
fun PoseStack.translate(x: Float, y: Float, z: Float) = translate(x.𝒹, y.𝒹, z.𝒹)

fun PoseStack.scale(x: Double, y: Double) = scale(x, y, 1.0)
fun PoseStack.scale(x: Float, y: Float) = scale(x, y, 1f)
fun PoseStack.scale(x: Double, y: Double, z: Double) = scale(x.𝒻, y.𝒻, z.𝒻)

@Suppress("DEPRECATION")
inline fun PoseStack.realize(thunk: () -> Unit) {
	RenderSystem.pushMatrix()
	RenderSystem.multMatrix(last().pose())
	thunk()
	RenderSystem.popMatrix()
}

object GL {
	val ONE                      = GL11.GL_ONE
	val ZERO                     = GL11.GL_ZERO

	val SRC_ALPHA                = GL11.GL_SRC_ALPHA
	val SRC_ALPHA_SATURATE       = GL11.GL_SRC_ALPHA_SATURATE
	val SRC_COLOR                = GL11.GL_SRC_COLOR
	val ONE_MINUS_SRC_ALPHA      = GL11.GL_ONE_MINUS_SRC_ALPHA
	val ONE_MINUS_SRC_COLOR      = GL11.GL_ONE_MINUS_SRC_COLOR

	val DST_ALPHA                = GL11.GL_DST_ALPHA
	val DST_COLOR                = GL11.GL_DST_COLOR
	val ONE_MINUS_DST_ALPHA      = GL11.GL_ONE_MINUS_DST_ALPHA
	val ONE_MINUS_DST_COLOR      = GL11.GL_ONE_MINUS_DST_COLOR

	val CONSTANT_ALPHA           = GL14.GL_CONSTANT_ALPHA
	val CONSTANT_COLOR           = GL14.GL_CONSTANT_COLOR
	val ONE_MINUS_CONSTANT_ALPHA = GL14.GL_ONE_MINUS_CONSTANT_ALPHA
	val ONE_MINUS_CONSTANT_COLOR = GL14.GL_ONE_MINUS_CONSTANT_COLOR

	val FRONT          = GL11.GL_FRONT
	val BACK           = GL11.GL_BACK
	val FRONT_AND_BACK = GL11.GL_FRONT_AND_BACK

	val FLAT   = GL11.GL_FLAT
	val SMOOTH = GL11.GL_SMOOTH

	val NEVER    = GL11.GL_NEVER
	val LESS     = GL11.GL_LESS
	val EQUAL    = GL11.GL_EQUAL
	val LEQUAL   = GL11.GL_LEQUAL
	val GREATER  = GL11.GL_GREATER
	val NOTEQUAL = GL11.GL_NOTEQUAL
	val GEQUAL   = GL11.GL_GEQUAL
	val ALWAYS   = GL11.GL_ALWAYS

	val QUADS = 7
}

interface RenderState {
	abstract fun enable()
	abstract fun disable()

	operator fun plus(other: RenderState) = of(
		{ this.enable(); other.enable(); },
		{ other.disable(); this.disable(); }
	)

	open operator fun not() = of(this::disable, this::enable)
		
	companion object {
		fun of(_enable: ()->Unit, _disable: ()->Unit) = object : RenderState {
			override fun enable() { _enable() }
			override fun disable() { _disable() }
		}
	}
}

fun RenderState.withDefault(default: ()->Unit) = DefaultedRenderState(this, default)
class DefaultedRenderState(
	private val parent: RenderState,
	private val _default: ()->Unit,
) : RenderState by parent {
	val default = parent + RenderState.of(_default, { })
	fun also(a: ()->Unit) = parent + RenderState.of(a, _default)
}

// Texture is assumed to be on most of the time
object TextureRenderState : RenderState {
	val force = RenderState.of(RenderSystem::enableTexture, RenderSystem::disableTexture)
	override fun enable() {}
	override fun disable() {}
	override operator fun not() = !force
}

object RenderStateDefs {
	@Suppress("DEPRECATION")
	fun color(c: RGBA) = RenderState.of(
		{ RenderSystem.color4f(c.r, c.g, c.b, c.a) },
		{ RenderSystem.color4f(1f, 1f, 1f, 1f) },
	)

	val blend = RenderState.of(RenderSystem::enableBlend, RenderSystem::disableBlend)
		.withDefault(RenderSystem::defaultBlendFunc)
	fun blend(a: Int, b: Int) = blend
		.also { RenderSystem.blendFunc(a, b) }
	fun blend(a: Int, b: Int, c: Int, d: Int) = blend
		.also { RenderSystem.blendFuncSeparate(a, b, c, d) }

	val texture = TextureRenderState
	fun texture(loc: ResourceLocation) = RenderState.of({ MC.textureManager.bind(loc) }, { })

	val cull = RenderState.of(RenderSystem::enableCull, RenderSystem::disableCull)

	@Suppress("DEPRECATION")
	val smooth = RenderState.of(
		{ RenderSystem.shadeModel(GL.SMOOTH) },
		{ RenderSystem.shadeModel(GL.FLAT) },
	)

	@Suppress("DEPRECATION")
	val alpha = RenderState.of(RenderSystem::enableAlphaTest, RenderSystem::disableAlphaTest)
		.withDefault(RenderSystem::defaultAlphaFunc)
	@Suppress("DEPRECATION")
	fun alpha(a: Int, b: Float) = alpha
		.also { RenderSystem.alphaFunc(a, b) }

	val depth = RenderState.of(RenderSystem::enableDepthTest, RenderSystem::disableDepthTest)
		.withDefault { RenderSystem.depthFunc(GL.LEQUAL) }
	fun depth(a: Int) = depth
		.also { RenderSystem.depthFunc(a) }
}

inline fun render(state: RenderStateDefs.()->RenderState, body: ()->Unit) {
	val state_ = state(RenderStateDefs)
	state_.enable()
	body()
	state_.disable()
}
