package kawaru.kokoro.internal

import java.io.InputStream
import java.nio.file.Files
import java.nio.file.FileSystems
import java.nio.file.Path
import java.nio.file.Paths
import java.util.function.Predicate
import java.util.function.Supplier

import com.google.gson.JsonParser

import mc.resources.ResourceLocation
import mc.server.packs.PackResources
import mc.server.packs.PackType
import mc.server.packs.AbstractPackResources
import mc.server.packs.metadata.MetadataSectionSerializer
import mc.server.packs.metadata.pack.PackMetadataSection
import mc.server.packs.repository.Pack
import mc.server.packs.repository.PackSource
import mc.server.packs.repository.PackRepository
import mc.server.packs.repository.RepositorySource

import kawaru.kokoro.util.*
import kawaru.kokoro.util.text.*

data class ModSpec(val path: Path, val name: String, val namespace: String)

private operator fun Path.div(s: String) = resolve(s)

private object KawaruResourcePack : PackResources {
	override fun getRootResource(s: String): InputStream? =
		if("/" in s || "\\" in s)
			throw IllegalArgumentException("Root resources can only be filenames, not paths (no / allowed!)")
		else ModSpec::class.java.classLoader.getResourceAsStream(s)

	override fun getResource(type: PackType, loc: ResourceLocation) =
		Files.newInputStream(Kokoro.mods[loc.namespace]!!.path / type.directory / loc.namespace / loc.path)

	override fun getResources(type: PackType, ns: String, dir: String, maxdepth: Int, predicate: Predicate<String>) =
		Kokoro.mods[ns]!!.let { mod ->
			if(!Files.exists(mod.path / type.directory / ns / dir)) listOf()
			else Files.walk(mod.path / type.directory / ns / dir, maxdepth).iterator().asSequence()
			.filter { !it.toString().endsWith(".mcmeta") }
			.filter { Files.isRegularFile(it) }
			.filter { predicate.test(it.fileName.toString()) }
			.map { ResourceLocation(ns, mod.path.relativize(it).toString().replace('\\', '/')) }
			.toList()
		}

	override fun hasResource(type: PackType, loc: ResourceLocation) =
		Files.exists(Kokoro.mods[loc.namespace]!!.path / type.directory / loc.namespace / loc.path)
	override fun getNamespaces(type: PackType) = Kokoro.mods.keys
	override fun <T> getMetadataSection(ser: MetadataSectionSerializer<T>): T? =
		if(ser === PackMetadataSection.SERIALIZER)
			PackMetadataSection(𝕊(Kokoro.mods.values.joinToString("・") { it.name }), 6) as? T
		else
			AbstractPackResources.getMetadataFromStream(ser, getRootResource("pack.mcmeta"))
	override fun getName() = "変"
	override fun close() {}
}

object Kokoro {
	val mods: Map<String, ModSpec> =
		Kokoro::class.java.classLoader.getResources("pack.mcmeta").asSequence()
			.mapNotNull { url ->
				JsonParser().parse(url.readText()).asJsonObject["kawaru"]?.asJsonObject
				?.let {
					if(url.toString().contains("!")) {
						val jar = java.net.URI.create(url.toString().split("!")[0])
						try {
							FileSystems.newFileSystem(jar, mapOf<String, Any>())
						} catch(e: Exception) {}
					}

					ModSpec(
						Paths.get(url.toURI()).parent,
						it["name"].asJsonPrimitive.asString,
						it["namespace"].asJsonPrimitive.asString
					)
				}
			}
			.associate{it.namespace to it}

	fun addResourcePacks(list: Array<RepositorySource>) = list +
		RepositorySource { packs, factory ->
			packs.accept(Pack.create(
				"kawaru", /*required*/ true,
				Supplier{KawaruResourcePack}, factory,
				Pack.Position.BOTTOM, PackSource.BUILT_IN
			))
		}
}
