package kawaru.tougou

import mc.client.gui.screens.recipebook.RecipeBookComponent
import mc.client.multiplayer.ClientLevel
import mc.client.multiplayer.ClientPacketListener
import mc.client.multiplayer.MultiPlayerGameMode as GameMode
import mc.client.player.LocalPlayer
import mc.core.BlockPos
import mc.core.Direction
import mc.network.chat.Component
import mc.network.protocol.Packet
import mc.network.protocol.game.ClientboundCommandSuggestionsPacket
import mc.network.protocol.game.ClientboundContainerAckPacket
import mc.network.protocol.game.ClientboundContainerClosePacket
import mc.network.protocol.game.ClientboundContainerSetContentPacket
import mc.network.protocol.game.ClientboundContainerSetDataPacket
import mc.network.protocol.game.ClientboundContainerSetSlotPacket
import mc.network.protocol.game.ClientboundHorseScreenOpenPacket
import mc.network.protocol.game.ClientboundOpenScreenPacket
import mc.network.protocol.game.ServerboundCommandSuggestionPacket
import mc.network.protocol.game.ServerboundContainerClosePacket
import mc.network.protocol.game.ServerboundUseItemOnPacket
import mc.world.InteractionHand
import mc.world.MenuProvider
import mc.world.entity.player.Player
import mc.world.inventory.AbstractContainerMenu as Menu
import mc.world.inventory.ChestMenu
import mc.world.inventory.ClickType
import mc.world.inventory.InventoryMenu
import mc.world.item.Item
import mc.world.item.ItemStack
import mc.world.level.Level
import mc.world.level.block.ChestBlock
import mc.world.level.block.TrappedChestBlock
import mc.world.level.block.entity.ChestBlockEntity
import mc.world.level.block.entity.RandomizableContainerBlockEntity
import mc.world.level.block.state.properties.ChestType
import mc.world.phys.BlockHitResult
import mc.world.phys.Vec3

import kawaru.kokoro.util.*

private typealias ClientPacket = Packet<mc.network.protocol.game.ClientGamePacketListener>
private typealias ServerPacket = Packet<mc.network.protocol.game.ServerGamePacketListener>

object RemoteCraft {
	var GameMode.consolidation: Consolidation by extraField { Consolidation(LEVEL, PLAYER) }
	fun RecipeBookComponent.initRemote() {
		if(menu is InventoryMenu) {
			MC.gameMode!!.consolidation.scanAll()
			MC.setScreen(MC.gameMode!!.consolidation.screen)
		}
	}

	fun handlePacket(p: ClientPacket) = MC.gameMode!!.consolidation.handlePacket(p)
}

interface BridgedItemStack {
	val stack: ItemStack
}

interface MenuBridge<BIS : BridgedItemStack> {
	val storageItems: List<BIS>
	fun clickStack(stack: BIS, type: ClickType)
	fun clickPlayerStack(slot: Int, type: ClickType)
}

data class SimpleMenuBridge(val menu: Menu) : MenuBridge<SimpleMenuBridge.SimpleBridgedItemStack> {
	data class SimpleBridgedItemStack(val slot: Int, override val stack: ItemStack) : BridgedItemStack

	override val storageItems = menu.slots.dropLast(36).map { SimpleBridgedItemStack(it.index, it.item) }

	override fun clickStack(stack: SimpleBridgedItemStack, type: ClickType) { }
	override fun clickPlayerStack(slot: Int, type: ClickType) { }
}

interface MenuLocation {
	val packet: ServerPacket
}

data class BlockMenuLocation(val pos: BlockPos) : MenuLocation {
	init { require(pos !is BlockPos.MutableBlockPos, {"pos must be immutable"}) }
	override val packet =
		BlockHitResult(Vec3.atCenterOf(pos), Direction.UP, pos, false)
		.let { hit -> ServerboundUseItemOnPacket(InteractionHand.MAIN_HAND, hit) }
}

class Consolidation(val level: Level, val player: LocalPlayer) {
	val RADIUS = 8

	private val syncer = Syncer(player)
	val screen = ConsolidateScreen(this, player.inventory)

	private val menus = mutableMapOf<MenuLocation, MenuBridge<*>>()
	private val storageItemsLoc = java.util.IdentityHashMap<ItemStack, Pair<MenuLocation, BridgedItemStack>>()

	fun storageItems(): List<ItemStack> {
		storageItemsLoc.clear()
		val list = mutableListOf<ItemStack>()
		for((loc, bridge) in menus.entries)
			for(bis in bridge.storageItems) {
				storageItemsLoc[bis.stack] = loc to bis
				list.add(bis.stack)
			}
		return list
	}

	fun scanAll() {
		menus.clear()
		val center = player.blockPosition()
		BlockPos.betweenClosed(
			center.offset(-RADIUS,-RADIUS,-RADIUS),
			center.offset(+RADIUS,+RADIUS,+RADIUS),
		).forEach(::scan)
	}
	
	fun scan(pos: BlockPos) {
		val pos = pos.immutable()
		val loc = BlockMenuLocation(pos)
		if(checkChest(pos)) {
			syncer.getMenu(loc) { menu, title ->
				when(menu) {
					is ChestMenu -> menus[loc] = SimpleMenuBridge(menu)
					else -> menus.remove(loc)
				}
				screen.dirty = true
			}
		} else if(menus.remove(loc) != null)
			screen.dirty = true
	}

	private fun checkChest(pos: BlockPos): Boolean {
		val bs = level.getBlockState(pos)
		val blk = bs.block as? ChestBlock ?: return false
		println("$pos $bs $blk")

		if(blk is TrappedChestBlock) return false

		if(blk.getMenuProvider(bs, level, pos) == null)
			return false

		if((level.getBlockEntity(pos) as? ChestBlockEntity)?.stillValid(player) != true)
			return false

		if(bs.getValue(ChestBlock.TYPE) == ChestType.RIGHT) {
			val pos2 = pos.relative(ChestBlock.getConnectedDirection(bs))
			if((level.getBlockEntity(pos2) as? ChestBlockEntity)?.stillValid(player) == true)
				return false
		}

		return true
	}

	fun handlePacket(p: ClientPacket) = syncer.handlePacket(p)
}

private class Syncer(val player: LocalPlayer) {
	private var lastContainerId = 0
	private var syncId = 0x980000
	private val pending = ArrayDeque<Pair<Int, (Menu?, Component?)->Unit>>()
	private var pendingMenu: Pair<Menu, Component>? = null

	fun getMenu(loc: MenuLocation, callback: (Menu?, Component?)->Unit) {
		lastContainerId = (lastContainerId+1)%100+1
		player.connection.send(loc.packet)
		player.connection.send(ServerboundContainerClosePacket(lastContainerId))
		player.connection.send(ServerboundCommandSuggestionPacket(syncId, "\u0000"))
		pending.addLast(syncId to callback)
		syncId++
	}

	fun handlePacket(p: ClientPacket): Boolean {
		when(p) {
			is ClientboundHorseScreenOpenPacket -> lastContainerId = p.containerId
			is ClientboundOpenScreenPacket -> lastContainerId = p.containerId
		}

		return if(pending.isEmpty()) false
			else {
				fun onMenu(id: Int, body: Menu.()->Unit) =
					if(id == -1) {}
					else if(id == pendingMenu?.first?.containerId)
						body(pendingMenu!!.first)
					else
						println("Expected container id ${pendingMenu?.first?.containerId}, but got ${id}!")
				when(p) {
					is ClientboundOpenScreenPacket -> {
						if(pendingMenu != null)
							println("Got a new menu even though I already had one!")
						val menu = p.type?.create(p.containerId, player.inventory)
						if(menu == null)
							println("Failed to create menu.")
						else
							pendingMenu = menu to p.title
						player.connection.send(ServerboundContainerClosePacket(p.containerId))
					}
					is ClientboundContainerSetContentPacket -> onMenu(p.containerId) { setAll(p.items) }
					is ClientboundContainerSetSlotPacket    -> onMenu(p.containerId) { setItem(p.slot, p.item) }
					is ClientboundContainerSetDataPacket    -> onMenu(p.containerId) { setData(p.id, p.value) }
					is ClientboundCommandSuggestionsPacket  -> {
						// We've checked above that there is a pending
						val (expSyncId, callback) = pending.removeFirst()
						if(expSyncId == p.id) {
							val pendingMenu = pendingMenu
							if(pendingMenu != null) {
								callback(pendingMenu.first, pendingMenu.second)
							} else {
								callback(null, null)
							}
						} else
							println("Expected sync id ${"%06X".format(expSyncId)}, got ${"%06X".format(p.id)}!")
						pendingMenu = null
					}

					is ClientboundContainerClosePacket -> {}
					is ClientboundContainerAckPacket -> {}

					else -> error("Unexpected packet")
				}
				true
			}
	}
}
