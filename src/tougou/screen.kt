package kawaru.tougou

import com.mojang.blaze3d.systems.RenderSystem
import com.mojang.blaze3d.vertex.PoseStack
import mc.client.gui.screens.inventory.AbstractContainerScreen
import mc.resources.ResourceLocation
import mc.world.Container
import mc.world.SimpleContainer
import mc.world.entity.player.Inventory
import mc.world.entity.player.Player
import mc.world.inventory.AbstractContainerMenu
import mc.world.inventory.ClickType
import mc.world.inventory.MenuType
import mc.world.inventory.Slot
import mc.world.item.ItemStack
import mc.core.Registry

import kawaru.kokoro.util.text.*
import kawaru.kokoro.util.draw.*

class ConsolidateScreen(val consolidation: Consolidation, inv: Inventory)
: AbstractContainerScreen<ConsolidateMenu>(
	ConsolidateMenu(inv, consolidation),
	inv,
	𝕋("kawaru-tougou:container.consolidate"),
) {
	val CONTAINER_BACKGROUND = ResourceLocation("textures/gui/container/generic_54.png")
	var dirty = true

	init {
		imageHeight = 222
		inventoryLabelY = imageHeight - 94
	}

	override fun tick() {
		if(dirty) {
			dirty = false
			val stacks = consolidation.storageItems().groupBy { it.item }
			menu.groups = stacks.entries
				.map{(k, v) -> ItemStack(k, v.map{it.count}.sum())}
				.filter{!it.isEmpty()}
				.sortedBy({Registry.ITEM.getId(it.item)})
		}
		super.tick()
	}

	override fun render(pose: PoseStack, i: Int, j: Int, f: Float) {
		renderBackground(pose)
		super.render(pose, i, j, f)
		renderTooltip(pose, i, j)
	}

	override protected fun renderBg(pose: PoseStack, f: Float, i: Int, j: Int) {
		render({ texture(CONTAINER_BACKGROUND) }) {
			val x = (width - imageWidth) / 2
			val y = (height - imageHeight) / 2
			blit(pose, x, y, 0, 0, imageWidth, imageHeight)
		}
	}


	override protected fun slotClicked(slot: Slot, slotIdx: Int, button: Int, type: ClickType) {
		println("${slot.item} ${slot.index} $slotIdx $button $type")
	}
}

class ConsolidateMenu(
	val playerInv: Inventory,
	val consolidation: Consolidation
) : AbstractContainerMenu(null, -98) {
	var groups = listOf<ItemStack>()

	// The only members that are used are:
	// • slots
	// • canDragTo()
	// • canTakeItemForPickAll()
	init {
		var slotN = 0
		(0 until 6).forEach { row ->
			(0 until 8).forEach { col ->
				addSlot(object : Slot(null, slotN++, 8 + col*18, 18 + row*18) {
					override fun mayPlace(it: ItemStack) = false
					override fun getMaxStackSize() = 0x7FFFFFFF
					override fun getItem() = groups.getOrNull(index) ?: ItemStack.EMPTY
				})
			}
		}
	}

	init {
		val invStart = 222-94+12
		var slotN = 0
		(0 until 9).forEach { col ->
			addSlot(Slot(playerInv, slotN++, 8 + col*18, invStart + 3*18+4))
		}
		(0 until 3).forEach { row ->
			(0 until 9).forEach { col ->
				addSlot(Slot(playerInv, slotN++, 8 + col*18, invStart + row*18))
			}
		}
	}

	override fun stillValid(player: Player) = true

	override fun removed(player: Player) {
		super.removed(player)
	}
}
