package kawaru.utsuku

import mc.Util
import mc.client.Camera
import mc.client.multiplayer.ClientLevel
import mc.client.renderer.LevelRenderer
import mc.client.renderer.LightTexture
import mc.client.renderer.MultiBufferSource
import mc.client.resources.sounds.SimpleSoundInstance
import mc.client.resources.sounds.SoundInstance
import mc.client.resources.sounds.TickableSoundInstance
import mc.client.sounds.SoundEventListener
import mc.client.sounds.WeighedSoundEvents
import mc.core.BlockPos
import mc.network.chat.Component
import mc.sounds.SoundSource
import mc.util.Mth
import mc.world.level.LightLayer
import mc.world.phys.Vec3

import kawaru.kokoro.util.*
import kawaru.kokoro.util.draw.*
import kawaru.kokoro.util.text.*

data class Subtitle(
	val time: Long,
	val sound: SoundInstance,
	val text: Component,
	val blockPos: BlockPos?,
)

object Subtitles {
	init {
		MC.soundManager.addListener { sound, wse ->
			wse.subtitle?.let { sub ->
				val bpos = if(sound is SimpleSoundInstance && sound.source == SoundSource.BLOCKS)
					BlockPos(sound.x, sound.y, sound.z)
						.takeIf{ sound.x == it.x+0.5 && sound.y == it.y+0.5 && sound.z == it.z+0.5 }
				else null
				if(bpos != null) LEVEL.subtitles.removeAll { it.blockPos == bpos }
				LEVEL.subtitles.add(Subtitle(
					Util.getMillis(),
					sound,
					𝕋("kawaru-utsuku:subtitle.type.${sound.source.getName()}", sub),
					bpos
				))
			}
		}
	}

	private val ClientLevel.subtitles: MutableList<Subtitle> by extraField { java.util.LinkedList() }
	
	fun LevelRenderer.renderSubtitles(pose: PoseStack, cam: Camera, mbs: MultiBufferSource) {
		pose {
			val camPos = cam.position
			pose.translate(-camPos.x, -camPos.y, -camPos.z)

			val iter = level.subtitles.iterator()
			for(sub in iter) {
				val sound = sub.sound
				val fade = 1 - (Util.getMillis() - sub.time) / 2000f
				if(fade < 0 || sound is TickableSoundInstance && sound.isStopped) iter.remove()
				else pose {
					pose.translate(sound.x, sound.y, sound.z)
					pose.mulPose(cam.rotation())
					val sc = Mth.sqrt(Vec3(sound.x, sound.y, sound.z).distanceTo(camPos)) / 120
					pose.scale(-sc, -sc, sc)

					val fg = WHITE * fade

					val pos = BlockPos(sound.x, sound.y, sound.z)
					val blockLight = level.getBrightness(LightLayer.BLOCK, pos)
					val skyLight = level.getBrightness(LightLayer.SKY, pos)

					// XXX Okay what the actual *fuck*, Minecraft?
					// Font.adjustColor makes this fadeout much harder than
					// it needs to be.
					val mat = pose.last().pose()
					val w = FONT.width(sub.text) / 2f
					FONT.drawInBatch(sub.text, -w, 0f, fg.𝒾 or 0x04000000, false, mat, mbs, true, 0, LightTexture.pack(blockLight, skyLight))
				}
			}
		}
	}
}
