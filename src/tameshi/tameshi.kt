package kawaru.tameshi

import mc.network.protocol.Packet
import mc.network.Connection

import kawaru.kokoro.util.*

private fun repr(a: Any, namefunc: (String)->String): String {
	val cls = a::class.java
	val fields = generateSequence<Class<*>>({cls}) { it.superclass }
		.toList()
		.reversed()
		.flatMap{it.declaredFields.toList()}
		.apply{forEach{it.isAccessible=true}}
		.joinToString(", ") { "${it.name}=${it[a]}" }
	return("${namefunc(cls.name)}{$fields}")
}

public fun dumpPacket(p: Packet<*>, c: mc.network.Connection) {
	when(p) {
		is mc.network.protocol.game.ClientboundAddEntityPacket,
		is mc.network.protocol.game.ClientboundForgetLevelChunkPacket,
		is mc.network.protocol.game.ClientboundKeepAlivePacket,
		is mc.network.protocol.game.ClientboundLevelChunkPacket,
		is mc.network.protocol.game.ClientboundLightUpdatePacket,
		is mc.network.protocol.game.ClientboundMoveEntityPacket,
		is mc.network.protocol.game.ClientboundRecipePacket,
		is mc.network.protocol.game.ClientboundRotateHeadPacket,
		is mc.network.protocol.game.ClientboundSetEntityDataPacket,
		is mc.network.protocol.game.ClientboundSetEntityMotionPacket,
		is mc.network.protocol.game.ClientboundSetTimePacket,
		is mc.network.protocol.game.ClientboundSoundPacket,
		is mc.network.protocol.game.ClientboundUpdateAdvancementsPacket,
		is mc.network.protocol.game.ClientboundUpdateAttributesPacket,
		is mc.network.protocol.game.ClientboundUpdateRecipesPacket,

		is mc.network.protocol.game.ServerboundMovePlayerPacket,
		is mc.network.protocol.game.ServerboundKeepAlivePacket,
		-> return
	}

	if(c.receiving == mc.network.protocol.PacketFlow.CLIENTBOUND) {
		println(repr(p) {
			val name = it.removePrefix("mc.network.protocol.game.").removeSuffix("Packet")
			when {
				name.startsWith("Serverbound") -> " > ${name.substring(11)} "
				name.startsWith("Clientbound") -> "<  ${name.substring(11)} "
				else -> name
			}
		})
	}
}
