package kawaru.zu

import java.util.WeakHashMap

import com.mojang.blaze3d.vertex.DefaultVertexFormat
import com.mojang.math.Vector3f
import mc.client.KeyMapping
import mc.client.ToggleKeyMapping
import mc.client.multiplayer.MultiPlayerGameMode as GameMode
import mc.client.renderer.debug.DebugRenderer
import mc.client.renderer.texture.DynamicTexture
import mc.core.BlockPos
import mc.core.BlockPos.MutableBlockPos
import mc.core.Vec3i
import mc.resources.ResourceLocation
import mc.tags.FluidTags
import mc.util.Mth
import mc.world.level.BlockGetter
import mc.world.level.ChunkPos
import mc.world.level.Level
import mc.world.level.block.Blocks
import mc.world.level.chunk.ChunkAccess
import mc.world.level.chunk.LevelChunk
import mc.world.level.material.MaterialColor
import mc.world.phys.AABB

import kawaru.zu.marker.*
import kawaru.kokoro.util.*
import kawaru.kokoro.util.draw.*


val KEY_CAVEMAP = ToggleKeyMapping("kawaru-zu:key.cavemap", 'M'.𝒾, "key.categories.misc") { true }
val KEYS = arrayOf<KeyMapping>(KEY_CAVEMAP)

object Minimap {
	val size = 128

	private val mapIcons = ResourceLocation("textures/map/map_icons.png");
	private val mapBG = ResourceLocation("textures/map/map_background.png")
	private val mapTextureBuf = DynamicTexture(size, size, true)
	private val mapTexture = MC.textureManager.register("kawaru-zu-minimap", mapTextureBuf)

	private var map: Map = DefaultMap(cavemap=KEY_CAVEMAP::isDown, biomeColors=true)

	fun renderMap(pose: PoseStack) {
		val cam = (MC.cameraEntity ?: PLAYER).position().let{(x,y,z)->Vec3i(x,y,z)}
		profile("minimap") {
			val s = size

			render({ alpha.default + texture(mapBG) }) {
				tessellate(pose, DefaultVertexFormat.POSITION_TEX) {
					vertex(0-7, s+7).uv(0f, 1f).end
					vertex(s+7, s+7).uv(1f, 1f).end
					vertex(s+7, 0-7).uv(1f, 0f).end
					vertex(0-7, 0-7).uv(0f, 0f).end
				}
			}

			profile("create") {
				val x = cam.x-size/2
				val z = cam.z-size/2
				(0 until size).forEach { px ->
					(0 until size).forEach { pz ->
						val argb = map.color(LEVEL, x+px, z+pz)
						val abgr = (
							   (argb and 0xFF00FF00L.𝒾)
							or (argb and 0x00FF0000 ushr 16)
							or (argb and 0x000000FF shl 16)
						)
						mapTextureBuf.pixels!!.setPixelRGBA(px, pz, abgr)
					}
				}
				mapTextureBuf.upload()
			}

			render({ blend.default + texture(mapTexture) }) {
				tessellate(pose, DefaultVertexFormat.POSITION_TEX) {
					vertex(0, s).uv(0f, 1f).end
					vertex(s, s).uv(1f, 1f).end
					vertex(s, 0).uv(1f, 0f).end
					vertex(0, 0).uv(0f, 0f).end
				}
			}

			profile("markers") {
				val markers = Markers.findMarkers(LEVEL)
					.mapNotNull { Markers.renderMarker(cam, size/2, it) }

				render({ blend.default + texture(mapIcons) }) {
					tessellate(pose, DefaultVertexFormat.POSITION_COLOR_TEX) {
						for(m in markers.reversed()) {
							pose {
								pose.translate(m.x.𝒹 + size/2, m.z.𝒹 + size/2, 0.𝒹)
								pose.mulPose(Vector3f.ZP.rotationDegrees(m.angle))
								pose.scale(4f, 4f, 3f)
								pose.translate(-0.125, 0.125, 0.0)

								val u0 = (m.shape % 16 + 0) / 16f;
								val v0 = (m.shape / 16 + 0) / 16f;
								val u1 = (m.shape % 16 + 1) / 16f;
								val v1 = (m.shape / 16 + 1) / 16f;
								vertex(-1, +1).color(m.color).uv(u0, v0).end
								vertex(+1, +1).color(m.color).uv(u1, v0).end
								vertex(+1, -1).color(m.color).uv(u1, v1).end
								vertex(-1, -1).color(m.color).uv(u0, v1).end
							}
						}
					}
				}
			}
		}
	}

	fun invalidate(chks: Array<Array<LevelChunk>>) {
		chks.forEach{it.forEach(ChunkSurfaces::invalidate)}
	}

	fun renderDebug(x: Double, y: Double, z: Double) {
		val p = 1.0/16
		val cpos = ChunkPos(BlockPos(x, y, z))
		render({ depth + !cull + blend.default + !texture }) {
			BlockPos.betweenClosed(
				cpos.minBlockX, 0, cpos.minBlockZ,
				cpos.maxBlockX, 0, cpos.maxBlockZ,
			).forEach { (xb, _, zb) ->
				ChunkSurfaces[LEVEL, xb, zb].forEach { yb ->
					DebugRenderer.renderFilledBox(
						AABB(
							xb+0+p, yb+1-p, zb+0+p,
							xb+1-p, yb+1+p, zb+1-p,
						).move(-x, -y, -z),
						1f, 0f, 0f, 0.5f,
					)
				}
			}
		}
	}
}

class ChunkSurfaces(
	private val surfaces: Array<List<Int>>,
) {
	operator fun get(x: Int, z: Int) = surfaces[z*16+x]

	companion object {
		private val cache = WeakHashMap<LevelChunk, ChunkSurfaces>()
		operator fun get(level: Level, x: Int, z: Int) = get(level.getChunkAt(pos(x,0,z)))[x and 15, z and 15]
		fun get(chunk: LevelChunk) = cache.getOrPut(chunk) { create(chunk) }
		fun invalidate(chunk: LevelChunk) = cache.remove(chunk)

		fun create(chunk: ChunkAccess) = ChunkSurfaces(run {
			BlockPos.betweenClosed(
				chunk.pos.minBlockX, 0, chunk.pos.minBlockZ,
				chunk.pos.maxBlockX, 0, chunk.pos.maxBlockZ,
			).map { (x, _, z) ->
				var last: Any? = null
				(chunk.maxBuildHeight downTo 0).filter { y ->
					val key = Pair(chunk.isEmpty(pos(x,y,z)), chunk.getFluidState(pos(x,y,z)).isEmpty)
					(last != key).also { last = key }
				}
				.drop(1).toList()
			}.toTypedArray()
		})
	}
}

fun BlockGetter.getMapColor(pos: BlockPos) = getBlockState(pos).getMapColor(this, pos)

fun BlockGetter.isEmpty(pos: BlockPos) = getMapColor(pos) === MaterialColor.NONE

interface Map {
	fun color(level: Level, x: Int, z: Int): Int
}

class DefaultMap(val cavemap: ()->Boolean, val biomeColors: Boolean) : Map {
	override fun color(level: Level, x: Int, z: Int): Int {
		val chunk = level.getChunk(pos(x, 0, z))
		val (color, pos, depth) = level.getTopBlock(x, z)
		return getMapColor(color, level, x, z, when {
			depth != null -> {
				// These 0.x are necessary or rounding will differ noticeably from vanilla
				val d = depth*0.1 + (x+z and 1)*0.2
				when { d < 0.5 -> 2; d > 0.9 -> 0; else -> 1 }
			}
			!chunk.isEmpty(pos.above()) -> 3
			else -> {
				val (_, pos2, _) = level.getTopBlock(x, z-1)
				compareValuesBy(pos, pos2) {it.y} + 1
			}
		})
	}

	fun Level.getTopBlock(x: Int, z: Int): Triple<MaterialColor, BlockPos, Int?> {
		val chunk = getChunkAt(pos(x, 0, z))
		val surf = ChunkSurfaces[this, x, z]
		var y = surf.firstOrNull { !chunk.isEmpty(pos(x,it,z)) } ?: -1

		if(cavemap()) {
			y = y.coerceAtMost(Mth.floor((MC.cameraEntity ?: PLAYER).y))
			if(chunk.isEmpty(pos(x,y,z)))
				y = surf.firstOrNull { it < y && !chunk.isEmpty(pos(x,it,z)) } ?: -1
		} else if(y == 127 && chunk[x,y,z].block === Blocks.BEDROCK) {
			y = surf.firstOrNull { it < y && !chunk.isEmpty(pos(x,it,z)) } ?: -1
			if(y == -1) {
				y = 127
				return Triple(
					getBiome(pos(x,y,z)).generationSettings.surfaceBuilderConfig
						.topMaterial.getMapColor(this, pos(x,y,z)),
					pos(x,y,z).immutable(),
					null
				)
			}
		}

		val depth = chunk.getFluidState(pos(x,y,z))
			.takeIf {
				it.`is`(FluidTags.WATER) ||
				dimension() == Level.NETHER && it.`is`(FluidTags.LAVA)
			}?.let {
				y - (surf.firstOrNull { it < y } ?: 0)
			}

		return Triple(
			chunk.getMapColor(pos(x,y,z)),
			pos(x,y,z).immutable(),
			depth
		)
	}

	fun getMapColor(color: MaterialColor, level: Level, x: Int, z: Int, shade: Int) =
		if(color === MaterialColor.NONE) ((x+z and 1) * 8 + 16) shl 24
		else {
			if(biomeColors) {
				val b = level.getBiome(pos(x,0,z))
				when(color) {
					MaterialColor.GRASS -> b.getGrassColor(x.𝒹, z.𝒹)
					MaterialColor.PLANT -> b.foliageColor.shade(191)
					MaterialColor.WATER -> b.waterColor
					else -> color.col
				}
			} else color.col
		}.shade(when(shade) { 0 -> 180; 1 -> 220; 2 -> 255; 3 -> 135; else -> 220})
}

object BiomeMap : Map {
	override fun color(level: Level, x: Int, z: Int): Int {
		val n = (x-1..x+1).sumBy{x0->(z-1..z+1).count{z0->level.getBiome(pos(x0,0,z0)).depth >= 0f}}
		if(level.getBiome(pos(x,0,z)).depth < 0f) {
			if(n < 1 && (z and 1) == 1) return 0
			return MaterialColor.COLOR_ORANGE.col.shade(when {
				n < 1 -> when((Mth.sin(z.𝒻) * 7 + x).𝒾 / 8 % 5) {
					0,4 -> 180; 1,3 -> 220; 2 -> 255; else -> 220
				}
				n < 3 -> 220; n < 5 -> 180; n < 7 -> 180; else -> 135
			})
		} else {
			if(n < 5) return MaterialColor.COLOR_BROWN.col.shade(220)
			if(n < 9) return MaterialColor.COLOR_BROWN.col.shade(135)
			return 0
		}
	}
}

private fun Int.shade(br: Int): Int {
	val r = (this and 0xFF0000) * br / 0xFF and 0xFF0000
	val g = (this and 0x00FF00) * br / 0xFF and 0x00FF00
	val b = (this and 0x0000FF) * br / 0xFF and 0x0000FF
	return (0xFF shl 24) or r or g or b
}
