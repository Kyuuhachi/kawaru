package kawaru.zu.marker

import mc.client.multiplayer.ClientLevel
import mc.core.Vec3i
import mc.world.entity.Entity
import mc.world.entity.animal.Animal
import mc.world.entity.item.ItemEntity
import mc.world.entity.monster.Enemy
import mc.world.entity.npc.Npc
import mc.world.entity.player.Player
import mc.world.entity.vehicle.MinecartChest
import mc.world.level.block.entity.BarrelBlockEntity
import mc.world.level.block.entity.BlockEntity
import mc.world.level.block.entity.ChestBlockEntity
import mc.world.level.block.entity.EnderChestBlockEntity
import mc.world.phys.Vec3
import mc.util.Mth

import kawaru.kokoro.util.*
import kawaru.kokoro.util.draw.*

data class Marker(
	val pos: Vec3,
	val shape: Int,
	val angle: Float?,
	val outShape: Int?,
	val outAngle: Boolean,
	val color: RGBA,
)

data class RenderMarker(
	val x: Int,
	val z: Int,
	val shape: Int,
	val angle: Float,
	val color: RGBA,
)

object Markers {
	fun findMarkers(level: ClientLevel): List<Marker> = sequence {
		// 0: arrow
		// 6: circle
		// 7: dot
		fun fromLivingEntity(e: Entity, trackOutside: Boolean, color: RGBA) =
			Marker(e.position(), 0, e.yRot, if(trackOutside) 6 else null, false, color)
		fun fromEntity(e: Entity, shape: Int, color: RGBA) =
			Marker(e.position(), shape, null, null, false, color)
		fun fromBlockEntity(e: BlockEntity, shape: Int, color: RGBA) =
			Marker(Vec3.atCenterOf(e.blockPos), shape, null, null, false, color)

		for(e in level.entitiesForRendering()) when(e) {
			is Player -> yield(fromLivingEntity(e, true,  RGBA(1.0f, 1.0f, 1.0f)))
			is Enemy  -> yield(fromLivingEntity(e, false, RGBA(1.0f, 0.5f, 0.5f)))
			is Animal -> yield(fromLivingEntity(e, false, RGBA(0.5f, 1.0f, 0.5f)))
			is Npc    -> yield(fromLivingEntity(e, false, RGBA(0.5f, 0.5f, 1.0f)))

			is ItemEntity    -> yield(fromEntity(e, 7, RGBA(1.0f, 1.0f, 0.5f)))
			is MinecartChest -> yield(fromEntity(e, 6, RGBA(1.0f, 1.0f, 0.5f)))
		}
		for(e in level.blockEntityList) when(e) {
			is ChestBlockEntity      -> yield(fromBlockEntity(e, 6, RGBA(1.0f, 1.0f, 0.5f)))
			is BarrelBlockEntity     -> yield(fromBlockEntity(e, 6, RGBA(1.0f, 1.0f, 0.5f)))
			is EnderChestBlockEntity -> yield(fromBlockEntity(e, 6, RGBA(1.0f, 0.3f, 1.0f)))
		}
	}.toList()

	fun renderMarker(cam: Vec3i, r: Int, m: Marker): RenderMarker? {
		val mx = Mth.floor(m.pos.x - cam.x)
		val mz = Mth.floor(m.pos.z - cam.z)
		val isInside = mx in -r..r && mz in -r..r

		val shape =
			if(isInside) m.shape
			else if(m.outShape != null) m.outShape
			else return null

		val angle =
			if(isInside) m.angle?:0f
			else if(m.outAngle) Mth.atan2(mz.𝒹, mx.𝒹).𝒻/PI*180-90
			else 0f

		val color = m.color * (1 - Math.abs(m.pos.y - cam.y)/32).coerceAtLeast(0.25).𝒻

		return RenderMarker(
			mx.coerceIn(-r, r),
			mz.coerceIn(-r, r),
			shape, angle, color,
		)
	}
}
