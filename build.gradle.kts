allprojects {
	tasks.withType<JavaExec>().configureEach {
		workingDir = rootProject.projectDir
	}
}

repositories {
	mavenCentral()
}

val mods by configurations.creating
val mcMods: String by project
mcMods.split(",").forEach { dependencies.add("mods", project(":$it")) }

fun JavaExec.isMinecraft() {
	val vanilla = tasks.getByPath(":minecraft:runVanilla") as JavaExec
	args(vanilla.args)
	argumentProviders.addAll(vanilla.argumentProviders)
	jvmArgs(vanilla.jvmArgs)
	classpath(vanilla.classpath)
	environment(vanilla.environment)
	mainClass.set(vanilla.mainClass)
	workingDir(vanilla.workingDir)
}

tasks.register<JavaExec>("run") {
	group = "mod"
	description = "Executes Minecraft with mods"

	isMinecraft()
	args = listOf(main!!) + args!!
	main = "kawaru.tsukuro.Main"
	classpath(mods)
}

tasks.register<JavaExec>("run2") {
	group = "mod"
	description = "Executes Minecraft with mods using a different injection mechanism"

	isMinecraft()
	args = listOf(main!!) + args!!
	main = "kawaru.tsukuro.Main2"
	classpath(mods)
}

gradle.taskGraph.whenReady {
	allTasks.forEach {
		println(it.path + " → " + it.taskDependencies.getDependencies(it).joinToString{it.path})
	}
}
