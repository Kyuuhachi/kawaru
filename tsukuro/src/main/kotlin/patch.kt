package kawaru.tsukuro

import org.objectweb.asm.tree.*
import org.objectweb.asm.Opcodes
import org.objectweb.asm.Handle
import org.objectweb.asm.util.*

sealed class Patch {
	abstract val cls : String

	abstract fun apply(node: ClassNode)
}

data class BytecodePatch(override val cls : String, val name : String, val desc : String, val lines : List<Line>) : Patch() {
	data class Line(val type : Type, val op : Op) {
		enum class Type { KEEP, ADD, DELETE }
	}

	override fun apply(node: ClassNode) { applyBytecodePatch(node) }
}

sealed class Op

sealed class Insn : Op() {
	abstract val opcode : Int
	data class OField (override val opcode : Int, val cls : String, val name : String, val desc : String) : Insn()
	data class OMethod(override val opcode : Int, val cls : String, val name : String, val desc : String) : Insn()
	data class OInsn  (override val opcode : Int) : Insn()
	data class OLdc   (override val opcode : Int, val cst : Any) : Insn()
	data class OVar   (override val opcode : Int, val idx : Int) : Insn()
	data class OInt   (override val opcode : Int,                val i : Int) : Insn()
	data class OIinc  (override val opcode : Int, val idx : Int, val i : Int) : Insn()
	data class OJump  (override val opcode : Int, val label : String) : Insn()
	data class OType  (override val opcode : Int, val type : String) : Insn()
	data class OIndy  (override val opcode : Int, val name : String, val desc : String, val bsm : Handle, val args : Array<Any>?) : Insn()
}
data class Label(val label : String) : Op()
data class LineNumber(val line : Int?, val label : String) : Op()
object Ellipsis : Op() { override fun toString() = "Ellipsis" }
object StartAnchor : Op() { override fun toString() = "StartAnchor" }
object EndAnchor : Op() { override fun toString() = "EndAnchor" }

data class FlagPatch(override val cls : String, val plus : Int, val minus : Int, val member : Member) : Patch() {
	sealed class Member {
		object Class : Member()
		data class Field(val name : String, val desc : String) : Member()
		data class Method(val name : String, val desc : String) : Member()
	}
	override fun apply(node: ClassNode) { applyFlagPatch(node) }
}

private fun Op.match(insn : AbstractInsnNode, labels : MutableMap<String, LabelNode>) = when(this) {
	is Insn -> opcode == insn.opcode && when(this) {
		is Insn.OField  -> insn is FieldInsnNode  && insn.owner == cls && insn.name == name && insn.desc == desc
		is Insn.OMethod -> insn is MethodInsnNode && insn.owner == cls && insn.name == name && insn.desc == desc
		is Insn.OInsn   -> insn is InsnNode
		is Insn.OLdc    -> insn is LdcInsnNode    && insn.cst == cst
		is Insn.OVar    -> insn is VarInsnNode    && insn.`var` == idx
		is Insn.OInt    -> insn is IntInsnNode    && insn.operand == i
		is Insn.OIinc   -> insn is IincInsnNode   && insn.`var` == idx && insn.incr == i
		is Insn.OJump   -> insn is JumpInsnNode   && labels.getOrPut(label, {insn.label}) === insn.label
		is Insn.OType   -> insn is TypeInsnNode   && insn.desc == type
		is Insn.OIndy   -> insn is InvokeDynamicInsnNode && insn.name == name && insn.desc == desc && insn.bsm == bsm && false // TODO
	}
	is Label -> insn is LabelNode && labels.getOrPut(label, {insn}) === insn
	is LineNumber -> insn is LineNumberNode && (line == null || insn.line == line) && labels.getOrPut(label, {insn.start}) === insn.start
	is Ellipsis -> true
	is StartAnchor -> insn.previous == null
	is EndAnchor -> insn.next == null
}

private fun Op.createInsn(labels : MutableMap<String, LabelNode>) = when(this) {
	is Insn -> when(this) {
		is Insn.OField  -> FieldInsnNode(opcode,  cls, name, desc)
		is Insn.OMethod -> MethodInsnNode(opcode, cls, name, desc, opcode == Opcodes.INVOKEINTERFACE)
		is Insn.OInsn   -> InsnNode(opcode)
		is Insn.OLdc    -> LdcInsnNode(cst)
		is Insn.OVar    -> VarInsnNode(opcode,  idx)
		is Insn.OInt    -> IntInsnNode(opcode,  i)
		is Insn.OIinc   -> IincInsnNode(idx, i)
		is Insn.OJump   -> JumpInsnNode(opcode, labels.getOrPut(label, ::LabelNode))
		is Insn.OType   -> TypeInsnNode(opcode, type)
		is Insn.OIndy   -> InvokeDynamicInsnNode(name, desc, bsm, null) // TODO
	}
	is Label -> labels.getOrPut(label, ::LabelNode)
	is LineNumber -> LineNumberNode(line ?: throw IllegalArgumentException("Can't insert unknown line number"), labels.getOrPut(label, ::LabelNode))
	is Ellipsis -> throw IllegalArgumentException("Can't insert ellipsis")
	is StartAnchor -> throw IllegalArgumentException("Can't insert start anchor")
	is EndAnchor -> throw IllegalArgumentException("Can't insert end anchor")
}

private operator fun List<MethodNode>.get(name : String, desc : String) = singleOrNull{it.name == name && it.desc == desc}
private operator fun List<FieldNode>.get(name : String, desc : String) = singleOrNull{it.name == name && it.desc == desc}

private fun findMatch(insns : InsnList, ops : List<Op>, name : String) : Pair<List<List<AbstractInsnNode>>, Map<String, LabelNode>> {
	data class Match(
		val index : Int,
		val range : List<AbstractInsnNode>,
		val offsets : List<List<AbstractInsnNode>>,
		val labels : Map<String, LabelNode>)

	require(ops.size > 0, {"No context for $name"})

	val matches = sequence {
		insns.iterator().asSequence().fold(sequence<Match>{}) { acc, insn ->
			val cur = acc + Match(0, listOf(insn), listOf(), mapOf())
			cur.filter { it.index == ops.size }.forEach{yield(it)}
			sequence {
				for(m in cur.filter{ it.index < ops.size }) {
					if(ops[m.index] == Ellipsis)
						yield(Match(m.index, m.range+insn.next, m.offsets, m.labels))
					val labels = m.labels.toMutableMap()
					if(ops[m.index].match(insn, labels))
						yield(Match(m.index+1, listOf(insn.next), m.offsets.plusElement(m.range), labels))
				}
			}
		}.filter { it.index == ops.size }.forEach{yield(it)}
	}.toList()

	val dump by lazy {
		java.io.StringWriter().also {
			it.write("\n")
			Textifier().also {
				insns.accept(TraceMethodVisitor(it))
			}.print(java.io.PrintWriter(it))
		}.toString()
	}

	require(matches.size <= 1, {"Multiple matches for $name: " +
		matches.map{insns.indexOf(it.offsets.first().first())..insns.indexOf(it.offsets.last().last())}
			.joinToString(", ", "[", "]", 8) + dump})
	require(matches.size >= 1, {"No match for $name" + dump})

	return Pair(matches.single().offsets, matches.single().labels)
}

private fun BytecodePatch.applyBytecodePatch(cls : ClassNode) {
	val method = cls.methods[name, desc] ?: throw IllegalArgumentException("Attempting to patch unknown method ${cls.name}.$name$desc")
	val ops = lines.filter{it.type != BytecodePatch.Line.Type.ADD}.map{it.op}

	val (ranges0, labels0) = findMatch(method.instructions, ops, "${cls.name}.$name$desc")
	val ranges = ranges0.iterator()
	val labels = labels0.toMutableMap()

	val head = LabelNode()
	method.instructions.insertBefore(ranges0.first().first(), head)
	lines.fold(head) { last : AbstractInsnNode, (type, op) ->
		when(type) {
			BytecodePatch.Line.Type.ADD -> op.createInsn(labels).also { method.instructions.insert(last, it) }
			BytecodePatch.Line.Type.DELETE -> last.also {ranges.next().forEach(method.instructions::remove)}
			BytecodePatch.Line.Type.KEEP -> ranges.next().last()
		}
	}
	method.instructions.remove(head)
	require(!ranges.hasNext(), {"Something went very wrong: ranges not exhausted"})
}

private fun FlagPatch.applyFlagPatch(cls : ClassNode) {
	fun mod(acc : Int) = acc or plus and minus.inv()
	when(member) {
		is FlagPatch.Member.Class -> cls.apply{access = mod(access)}
		is FlagPatch.Member.Field -> cls.fields[member.name, member.desc]?.apply{access = mod(access)} ?: throw IllegalArgumentException("Attempting to reflag unknown field ${cls.name}.${member.name} : ${member.desc}")
		is FlagPatch.Member.Method -> cls.methods[member.name, member.desc]?.apply{access = mod(access)} ?: throw IllegalArgumentException("Attempting to reflag unknown method ${cls.name}.${member.name}${member.desc}")
	}
}
