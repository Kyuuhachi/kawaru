package kawaru.tsukuro

import org.objectweb.asm.util.Printer
import org.objectweb.asm.Opcodes.*

private val CLASS_ACC = mapOf(
	"public"     to 0x0001,
	"final"      to 0x0010,
	"super"      to 0x0020,
	"interface"  to 0x0200,
	"abstract"   to 0x0400,
	"synthetic"  to 0x1000,
	"annotation" to 0x2000,
	"enum"       to 0x4000
)
private val FIELD_ACC = mapOf(
	"public"    to 0x0001,
	"private"   to 0x0002,
	"protected" to 0x0004,
	"static"    to 0x0008,
	"final"     to 0x0010,
	"volatile"  to 0x0040,
	"transient" to 0x0080,
	"synthetic" to 0x1000,
	"enum"      to 0x4000
)
private val METHOD_ACC = mapOf(
	"public"       to 0x0001,
	"private"      to 0x0002,
	"protected"    to 0x0004,
	"static"       to 0x0008,
	"final"        to 0x0010,
	"synchronized" to 0x0020,
	"bridge"       to 0x0040,
	"varargs"      to 0x0080,
	"native"       to 0x0100,
	"abstract"     to 0x0400,
	"strict"       to 0x0800,
	"synthetic"    to 0x1000
)

internal class Parser(val s : String) {
	private var x = 0

	private fun fail(what : String) : Nothing {
		val lines = s.take(x+1).split("\n")
		throw IllegalArgumentException("Expected $what at ${lines.size}:${lines.last().length}")
	}

	private inner class Token<T>(val regex : Regex, val what : String, val transform : (String) -> T?) {
		override fun toString() = regex.pattern
		fun test() = regex.find(s, x)?.let { m ->
			if(m.range.start == x)
				transform(m.value).also { x = m.range.endInclusive + 1 }
			else null
		}
		operator fun invoke() = test() ?: fail(what)
		operator fun <Q> invoke(f : Token<T>.(T) -> Q?) = Token(regex, what, transform={ transform(it)?.let{f(it)} })
	}

	private operator fun String.get(what : String) = Token(Regex(this), what, {it})
	private val <T> T.w get() = also { WHITE() }

	private val CLASSNAME  = "[a-zA-Z0-9_$/]+(?:[a-zA-Z0-9_$/])"    ["CLASSNAME"]
	private val NAME       = "[<>a-zA-Z0-9_$]+(?![<>a-zA-Z0-9_$])"  ["NAME"]
	private val DESC       = "\\[*(?:[BCDFIJSZ]|L$CLASSNAME;)"      ["DESC"]
	private val MDESC      = "\\(\\s*(?:$DESC\\s*)*\\)(?:$DESC|V\\b)"   ["MDESC"] { it.replace(Regex("\\s"), "") }

	private val WHITE      = "(?:[ \t]+|/\\*[\\s\\S]*?\\*/)*(?://[^\n]*)?" ["WHITE"]
	private val WHITENL    = "(?:$WHITE\n)*$WHITE"                  ["WHITENL"]
	private val EOL        = "\n"                                   ["EOL"]
	private val INDENT     = "(?:$WHITE\n)*+[ \t]+"                 ["INDENT"]

	private val DOT        = "\\."                                  ["DOT"]
	private val COLON      = ":"                                    ["COLON"]
	private val ELLIPSIS   = "\\.\\.\\."                            ["ELLIPSIS"]
	private val START_ANCHOR="\\^"                                  ["START_ANCHOR"]
	private val END_ANCHOR = "\\$"                                  ["END_ANCHOR"]

	private val TYPE       = "[+-]?"                                ["TYPE"] { when(it) {
		""  -> BytecodePatch.Line.Type.KEEP
		"+" -> BytecodePatch.Line.Type.ADD
		"-" -> BytecodePatch.Line.Type.DELETE
		else -> fail(what) } }
	private val OPCODE     = "[A-Z_0-9]+\\b"                        ["OPCODE"] { Printer.OPCODES.indexOf(it).let { if(it == -1) null else it } }
	private val NAT        = "[0-9]+\\b"                            ["NAT"] { it.toNumber().toInt() }
	private val _INT       = "[+-]?(?:0[Xx][0-9a-fA-F]+|0[Oo][0-7]+|0[Bb][01]+|[0-9]+)"
	private val INT        = "$_INT\\b"                             ["INT"] { it.toNumber().toInt() }
	private val LONG       = "$_INT[Ll]\\b"                         ["LONG"] { it.dropLast(1).toNumber() }
	private val FLOAT      = "[+-]?[0-9]+(?:\\.[0.9]+)?[Ff]\\b"     ["FLOAT"] { it.dropLast(1).toFloat() }
	private val LABEL      = "@[a-zA-Z0-9_]+\\b"                    ["LABEL"] { it.drop(1) }

	private val LINENUMBER = "LINENUMBER\\b"                        ["LINENUMBER"]
	private val DONTCARE   = "\\*"                                  ["DONTCARE"]

	private val FLAGS      = "flags\\b"                             ["FLAGS"]
	private val FLAGTYPE   = "[+-]"                                 ["FLAGTYPE"]
	private val CLASSFLAG  = "\\w+\\b"                              ["CLASSFLAG"] { CLASS_ACC[it] }
	private val FIELDFLAG  = "\\w+\\b"                              ["FIELDFLAG"] { FIELD_ACC[it] }
	private val METHODFLAG = "\\w+\\b"                              ["METHODFLAG"] { METHOD_ACC[it] }

	private fun CLASS_OR_DESC() = DESC.test() ?: CLASSNAME()

	private fun parseBytecodePatch() = run {
		val funcs = sequence {
			do {
				val cls = CLASSNAME().w
				DOT().w
				val name = NAME().w
				val desc = MDESC().w
				EOL()
				yield(Triple(cls, name, desc))
			} while(INDENT.test() == null)
		}.toList()

		val ops = sequence {
			do {
				yield(BytecodePatch.Line(TYPE().w, parseOp()).also { EOL() })
			} while(INDENT.test() != null)
		}.toList()

		funcs.map{ (cls,name,desc) -> BytecodePatch(cls, name, desc, ops) }
	}

	private fun parseOp() : Op {
		ELLIPSIS.test()?.w?.let { return Ellipsis }
		START_ANCHOR.test()?.w?.let { return StartAnchor }
		END_ANCHOR.test()?.w?.let { return EndAnchor }
		LABEL.test()?.w?.let { return Label(it) }
		LINENUMBER.test()?.w?.let { return LineNumber(if(DONTCARE.test()?.w != null) null else NAT().w, LABEL().w) }
		return when(val opcode = OPCODE().w) {
			in GETSTATIC..PUTFIELD ->
				Insn.OField(opcode, CLASSNAME().w.also{DOT()}.w, NAME().w.also{COLON().w}, DESC().w)
			in INVOKEVIRTUAL..INVOKEINTERFACE ->
				Insn.OMethod(opcode, CLASSNAME().w.also{DOT()}.w, NAME().w, MDESC().w)
			NOP, ARRAYLENGTH, ATHROW, MONITORENTER, MONITOREXIT,
			in ACONST_NULL..DCONST_1, in IALOAD..SALOAD, in IASTORE..SASTORE, in POP..SWAP,
			in IADD..LXOR, in I2L..I2S, in LCMP..DCMPG, in IRETURN..RETURN ->
				Insn.OInsn(opcode)
			in ILOAD..ALOAD, in ISTORE..ASTORE, RET ->
				Insn.OVar(opcode, NAT().w)
			in IFEQ..JSR, IFNULL, IFNONNULL ->
				Insn.OJump(opcode, LABEL().w)
			LDC ->
				Insn.OLdc(opcode, parseObject())
			BIPUSH, SIPUSH, NEWARRAY ->
				Insn.OInt(opcode, INT().w)
			IINC ->
				Insn.OIinc(opcode, NAT().w, INT().w)
			NEW, INSTANCEOF ->
				Insn.OType(opcode, CLASSNAME().w)
			ANEWARRAY, CHECKCAST ->
				Insn.OType(opcode, CLASS_OR_DESC().w)
			// INVOKEDYNAMIC ->
			// 	Insn.OIndy(opcode, HANDLE().w, MDESC().w, NAME().w, parseIndyArgs())
			// RET, MULTIANEWARRAY, TABLESWITCH, LOOKUPSWITCH
			else -> throw IllegalArgumentException(opcode.toString() + " " + "[^\n]*"["LINE"]())
		}
	}

	private fun parseObject() : Any {
		INT.test()?.let { return it }
		LONG.test()?.let { return it }
		FLOAT.test()?.let { return it }
		parseString()?.let { return it }
		fail("INT|LONG|FLOAT|STRING")
	}

	private fun parseIndyArgs() {

	}

	private fun String.toNumber() =
		when {
			startsWith("0x", true) -> drop(2).toLong(16)
			startsWith("0o", true) -> drop(2).toLong(8)
			startsWith("0b", true) -> drop(2).toLong(2)
			else -> toLong()
		}

	private fun parseString() =
		if(s[x] == '"') {
			x++
			generateSequence {
				when(val c = s[x++]) {
					'"' -> null
					'\n' -> fail("STRCHAR")
					'\\' -> when(s[x++]) {
						'"' -> '\"'
						'\\' -> '\\'
						'\'' -> '\''
						'n' -> '\n'
						't' -> '\t'
						'r' -> '\r'
						'x' -> s.substring(x, x+2).toIntOrNull(16)?.toChar()?.also{x+=2} ?: fail("2HEXDIGIT")
						'u' -> s.substring(x, x+4).toIntOrNull(16)?.toChar()?.also{x+=4} ?: fail("4HEXDIGIT")
						'U' -> s.substring(x, x+8).toIntOrNull(16)?.toChar()?.also{x+=8} ?: fail("8HEXDIGIT")
						else -> fail("ESCAPE")
					}
					else -> c
				}
			}.joinToString("")
		} else null

	private fun parseFlags() : FlagPatch {
		val cls = CLASSNAME().w
		val member = if(DOT.test()?.w != null) {
			val name = NAME().w
			if(COLON.test()?.w != null)
				FlagPatch.Member.Field(name, DESC().w)
			else
				FlagPatch.Member.Method(name, MDESC().w)
		} else FlagPatch.Member.Class
		EOL()
		INDENT()
		val token : Token<Int> = when(member) {
			is FlagPatch.Member.Class -> CLASSFLAG
			is FlagPatch.Member.Field -> FIELDFLAG
			is FlagPatch.Member.Method -> METHODFLAG
		}
		var plus = 0
		var minus = 0
		while(EOL.test() == null) {
			val type = FLAGTYPE().w
			val flag = token().w
			when(type) {
				"+" -> plus = plus or flag
				"-" -> minus = minus or flag
			}
		}
		return FlagPatch(cls, plus, minus, member)
	}

	fun parse() =
		sequence {
			WHITENL()
			while(x < s.length) {
				if(FLAGS.test()?.w != null)
					yield(parseFlags())
				else
					yieldAll(parseBytecodePatch())
				WHITENL()
			}
		}.toList()
}
