package kawaru.tsukuro

import java.net.URL
import java.nio.file.Files
import java.nio.file.Paths
import java.util.jar.JarInputStream

import org.objectweb.asm.ClassReader
import org.objectweb.asm.ClassWriter
import org.objectweb.asm.tree.ClassNode

object Tsukuro {
	fun parse(s: String): List<Patch> = Parser(s).parse()

	fun patch(patchset: List<Patch>, bytes: ByteArray, cldr: ClassLoader? = null)
		= patch(patchset, bytes,
			if(cldr != null)
				object : ClassWriter(ClassWriter.COMPUTE_FRAMES) {
					override fun getCommonSuperClass(a: String, b: String): String {
						val ca = Class.forName(a.replace("/", "."), false, cldr)
						val cb = Class.forName(b.replace("/", "."), false, cldr)
						return when {
							ca.isAssignableFrom(cb) -> a
							cb.isAssignableFrom(ca) -> b
							ca.isInterface || cb.isInterface -> "java/lang/Object"
							else -> generateSequence(ca){it.superclass}
								.first{it.isAssignableFrom(cb)}
								.name.replace(".", "/")
						}
					}
				}
			else
				ClassWriter(ClassWriter.COMPUTE_FRAMES)
		)

	fun patch(patchset: List<Patch>, bytes: ByteArray, writer: ClassWriter): ByteArray {
		val cls = ClassNode().apply { ClassReader(bytes).accept(this, ClassReader.SKIP_FRAMES) }
		patchset.filter { it.cls==cls.name }.forEach { it.apply(cls) }
		cls.accept(writer)
		return writer.toByteArray()
	}

	fun findPatches(urls: List<URL>): Map<String, List<Patch>> =
		urls.flatMap { url ->
			if(url.toString().endsWith(".jar"))
				JarInputStream(url.openStream()).use { jar ->
					generateSequence{jar.nextJarEntry}
						.filter { !it.isDirectory && it.name.endsWith(".tkr") }
						.flatMap { Tsukuro.parse(jar.bufferedReader().readText()) }
						.toList()
				}
			else
				Files.walk(Paths.get(url.toURI()))
					.iterator().asSequence()
					.filter { Files.isRegularFile(it) && it.toString().endsWith(".tkr") }
					.flatMap { Tsukuro.parse(Files.newBufferedReader(it).readText()) }
					.toList()
		}.groupBy{it.cls}
}
