package kawaru.tsukuro

import java.io.File
import java.net.URL
import java.net.URLClassLoader
import java.util.jar.*

import kawaru.tsukuro.Tsukuro
import kawaru.tsukuro.Patch

object Main {
	@JvmStatic
	fun main(args_: Array<String>) {
		val urls = System.getProperty("java.class.path")
			.split(File.pathSeparator)
			.map { File(it).toURI().toURL() }

		val loader = TsukuroClassLoader(urls, Tsukuro.findPatches(urls), object{}::class.java.classLoader)

		val args = args_.toMutableList()
		Class.forName(args.removeAt(0), false, loader)
			.getMethod("main", Array<String>::class.java)
			.invoke(null, args.toTypedArray())
	}

	val LOADER_EXCLUSIONS = listOf("java.", "sun.", "com.lwjgl.", "org.apache.logging.")

	class TsukuroClassLoader(
		val urls: List<URL>,
		val patches: Map<String, List<Patch>>,
		val fallback: ClassLoader
	) : URLClassLoader(urls.toTypedArray(), null) {
		override fun addURL(url: URL) = throw NotImplementedError()

		override fun findClass(name: String): Class<*> {
			if(LOADER_EXCLUSIONS.any{name.startsWith(it)})
				return fallback.loadClass(name)

			this.patches.get(name.replace('.', '/'))?.let { ps ->
				val bytes = findResource(name.replace('.', '/') + ".class").readBytes()
				val bytes2 = Tsukuro.patch(ps, bytes)
				return defineClass(name, bytes2, 0, bytes2.size, null as java.security.CodeSource?)
			}

			return super.findClass(name)
		}
	}
}

object Main2 {
	@JvmStatic
	fun main(args_: Array<String>) {
		val ucl = object{}::class.java.classLoader as URLClassLoader
		val ucpFd = URLClassLoader::class.java.getDeclaredField("ucp")
			.apply { isAccessible = true; }
		val ucp = ucpFd[ucl] as sun.misc.URLClassPath

		val outfile = File.createTempFile("minecraft-", ".jar")
			.apply { deleteOnExit() }
			.apply {
				val t1 = System.currentTimeMillis()
				JarOutputStream(outputStream()).use { ozip ->
					for((k, ps) in Tsukuro.findPatches(ucp.getURLs().toList())) {
						val path = k + ".class"
						ucp.findResource(path, false)?.let {
							ozip.putNextEntry(JarEntry(path))
							ozip.write(Tsukuro.patch(ps, it.readBytes()))
							ozip.closeEntry()
						}
					}
				}
				val t2 = System.currentTimeMillis()
				println("Created patch in ${t2-t1}ms")
			}
			.toURI().toURL()

		ucpFd[ucl] = sun.misc.URLClassPath(arrayOf(outfile) + ucp.getURLs())
		ucp.closeLoaders()

		val args = args_.toMutableList()
		Class.forName(args.removeAt(0))
			.getMethod("main", Array<String>::class.java)
			.invoke(null, args.toTypedArray())
	}
}
