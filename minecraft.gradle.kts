buildDir = File(rootProject.projectDir, ".build/${project.name}")

val mcVersion = System.getenv("MC_VERSION") ?: run { val mcVersion: String by project; mcVersion }

val download by registerTask<DownloadMC>("download") {
	description = "Downloads assets and libraries"

	version.set(mcVersion)
}

val deobf by registerTask<Deobfuscate>("deobfuscate") {
	description = "Unapplies the Retroguard mappings"

	injar.set(download.client)
	inheritance.from(download.client)
	mappings.set(download.clientMappings)
}

val abbreviate by registerTask<Abbreviate>("abbreviate") {
	description = "Renames the `net/minecraft` package to `mc`"

	injar.set(deobf.outjar)
}

val disasm by registerTask<Disassemble>("disasm") {
	description = "Disassembles the Minecraft jar into bytecode"

	injar.set(abbreviate.outjar)
	outdir.set(rootProject.file("decomp/$mcVersion/asm"))
}

val decompile by registerTask<Decompile>("decompile") {
	description = "Decompiles the Minecraft jar into Java source"

	injar.set(abbreviate.outjar)
	libs.from(download.libs)
	outdir.set(rootProject.file("decomp/$mcVersion/src"))
}

val decompileBoth by registerTask("decompileBoth") {
	description = "Convenience task running both ${disasm.name} and ${decompile.name}"
	dependsOn(disasm, decompile)
}

configurations.create("default")
dependencies.add("default", files(download.libs))
dependencies.add("default", files(abbreviate.outjar)) // Why? Isn't the artifact enough?
artifacts.add("default", abbreviate.outjar)

val runVanilla by registerTask<JavaExec>("runVanilla") {
	description = "Executes Minecraft, vanilla"

	classpath(project.files(abbreviate.outjar, download.libs))
	mainClass.set("mc.client.main.Main")

	args("--accessToken", "")
	argumentProviders.add{listOf("--assetsDir", download.assets.get().asFile.absolutePath)}
	args("--assetIndex", mcVersion)
	args("--version", mcVersion)
	args("--username", System.getenv("MC_USERNAME") ?: "Player")

	workingDir(File(rootProject.projectDir, ".minecraft"))
	workingDir.mkdirs()
}

inline fun <reified T : Task> registerTask(name: String, crossinline body: T.()->Unit) =
	tasks.register<T>(name) {
		this.group = "minecraft"
		body(this)
	}
