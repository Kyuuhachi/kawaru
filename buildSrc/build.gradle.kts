plugins {
	`kotlin-dsl`
}

buildDir = File("../.build", project.name)

repositories {
	gradlePluginPortal()
}

dependencies {
	implementation("org.jetbrains.kotlin:kotlin-gradle-plugin")
	implementation("com.fasterxml.jackson.core:jackson-databind:2.9.8")
	implementation("com.fasterxml.jackson.module:jackson-module-kotlin:2.9.8")

	implementation("org.ow2.asm:asm:7.2")
	implementation("org.ow2.asm:asm-commons:7.2")
	implementation("org.ow2.asm:asm-util:7.2")
	implementation("org.benf:cfr:0.151")

	implementation("kawaru:tsukuro")
	implementation("kawaru:takara")
}

tasks.withType<org.jetbrains.kotlin.gradle.tasks.KotlinCompile> {
	kotlinOptions.jvmTarget = "1.8"
}
