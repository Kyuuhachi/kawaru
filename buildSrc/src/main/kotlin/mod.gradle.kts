plugins {
	id("org.jetbrains.kotlin.jvm")
	`java-library`
}

val minecraft = evaluationDependsOn(":minecraft")

buildDir = File(rootProject.projectDir, ".build/${project.name}")

repositories {
	mavenCentral()
}

tasks.withType<org.jetbrains.kotlin.gradle.tasks.KotlinCompile> {
	kotlinOptions.jvmTarget = "1.8"
}

sourceSets {
	main {
		val dir = File(rootProject.projectDir, "src/${project.name}")
		java.setSrcDirs(listOf(dir))
		resources.setSrcDirs(listOf(dir))
		resources.exclude("**/*.kt")
		resources.exclude("**/*.java")
	}
}

dependencies {
	implementation(minecraft)
	compileOnly(files(tasks.create<TsukuroTask>("tsukuro") {
		input.srcDirs(sourceSets["main"].allSource.srcDirs)
		input.include("**/*.tkr")
		classpath.from(minecraft.configurations.default)
	}))
}

apply<kawaru.takara.TakaraPlugin>()
tasks.withType<Copy> {
	includeEmptyDirs = false
}

