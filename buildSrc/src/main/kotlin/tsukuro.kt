import org.gradle.api.*
import org.gradle.api.tasks.*
import org.gradle.api.file.*
import org.gradle.work.*

import org.gradle.api.file.FileCollection
import org.gradle.api.plugins.JavaPluginConvention

import java.net.URLClassLoader
import java.io.PrintWriter
import java.nio.file.Paths
import java.nio.file.Files
import java.nio.file.FileSystems
import java.util.zip.ZipInputStream

import org.objectweb.asm.ClassReader
import org.objectweb.asm.Type
import org.objectweb.asm.tree.ClassNode
import org.objectweb.asm.tree.analysis.Analyzer
import org.objectweb.asm.tree.analysis.AnalyzerException
import org.objectweb.asm.tree.analysis.BasicValue
import org.objectweb.asm.tree.analysis.BasicVerifier
import org.objectweb.asm.tree.analysis.Frame
import org.objectweb.asm.util.CheckClassAdapter
import org.objectweb.asm.util.Textifier
import org.objectweb.asm.util.TraceMethodVisitor
import org.objectweb.asm.Opcodes

import kawaru.tsukuro.Tsukuro

@CacheableTask
open class TsukuroTask : DefaultTask() {
	@InputFiles @Classpath val input = project.objects.sourceDirectorySet("tsukuro", "tsukuro input")
	@InputFiles @Classpath val classpath = project.objects.fileCollection()
	@OutputDirectory val output = project.objects.directoryProperty().convention(project.layout.buildDirectory.dir(name))

	@TaskAction fun run() {
		val cldr = URLClassLoader(classpath.files.map { it.toURI().toURL() }.toTypedArray())
		project.delete(output.get().asFile.listFiles())
		val patchset = input.flatMap{Tsukuro.parse(it.readText())}
		val files = patchset.map{it.cls.substringBefore('$')}.toSet()
		findClasses(files, classpath) { name, bytes ->
			val file = output.file(name).get().asFile
			file.parentFile.mkdirs()
			val patched = Tsukuro.patch(patchset, bytes, cldr)
			validate(patched)
			file.writeBytes(patched)
		}
	}
}

fun findClasses(files: Set<String>, classpath: FileCollection, body: (String, ByteArray) -> Unit) {
	sequence<Pair<String, ()->ByteArray>> {
		classpath.files
		.filter { it.exists() }
		.forEach { entry ->
			if(entry.path.endsWith(".jar"))
				ZipInputStream(entry.inputStream()).use { zip ->
					generateSequence{zip.nextEntry}
					.filter { !it.isDirectory }
					.forEach { yield(it.name to {zip.readBytes()}) }
				}
			else
				Files.walk(Paths.get(entry.toURI())).iterator().asSequence().map { it.toFile() }
				.filter { !it.isDirectory() }
				.forEach{ yield(it.relativeTo(entry).path to {it.readBytes()}) }
		}
	}
	.filter { (it, _) -> it.toString().endsWith(".class") }
	.filter { (it, _) -> it.removeSuffix(".class").substringBefore('$') in files }
	.forEach { (name, read) ->
		body(name, read())
	}
}

// TODO move this into Tsukuro itself
fun validate(bytes: ByteArray) {
	val cn = ClassNode().apply {
		ClassReader(bytes).accept(this, ClassReader.SKIP_DEBUG)
	}

	val a = Analyzer<BasicValue>(BasicVerifier())

	for(m in cn.methods)
		try {
			a.analyze(cn.name, m)
		} catch(e: AnalyzerException) {
			val index = e.node?.let(m.instructions::indexOf)
			val frames = a.frames.map { f -> dumpLocals(f, m.maxLocals) + " " + dumpStack(f, m.maxStack) }
			val asm = Textifier().apply{
				m.instructions.accept(TraceMethodVisitor(this))
			}.text.map{it.toString().trimEnd()}

			val lines = frames.zip(asm).mapIndexed { i, (f, op) ->
				"%s %04d %s%s\n".format(if(i==index) "#" else " ", i, f, op)
			}

			val sb = StringBuilder()
			sb.append(e.message).append("\n")
			sb.append("${cn.name}.${m.name}${m.desc} locals=${m.maxLocals}, stack=${m.maxStack}\n")
			if(index != null)
				lines.subList(
					(index-20).coerceAtLeast(0),
					(index+21).coerceAtMost(lines.size)
				).forEach{sb.append(it)}
			else
				lines.forEach{sb.append(it)}
			
			throw GradleException(sb.toString(), e)
		}
}

fun dumpLocals(f: Frame<BasicValue>?, n: Int) =
	if(f == null) "?".repeat(n)
	else StringBuilder().apply {
		var i = 0
		while(i < f.locals)
			i += appendType(f.getLocal(i))
		require(i == n)
	}.toString()

fun dumpStack(f: Frame<BasicValue>?, n: Int) =
	if(f == null) "?".repeat(n)
	else StringBuilder().apply {
		var i = 0
		var j = 0
		while(i < f.stackSize)
			j += appendType(f.getStack(i++))
		while(j++ < n)
			append(".")
	}.toString()

fun StringBuilder.appendType(v : BasicValue) =
	when(v.toString()) {
		"J" -> append("Ｊ")
		"D" -> append("Ｄ")
		else -> append(v.toString()[0])
	}.let { v.size }
