import org.gradle.api.*
import org.gradle.api.tasks.*
import org.gradle.api.file.RegularFile

import java.io.File
import java.util.zip.*

import org.objectweb.asm.*
import org.objectweb.asm.commons.*

import com.fasterxml.jackson.module.kotlin.*

data class Mapping(val name : String, val members : Map<Pair<String, String>, String>)
typealias Mappings = Map<String, Mapping>

fun Mappings.reverse() = entries.associate { (k, v) ->
	v.name to Mapping(k, v.members.entries.associate { (k, v) ->
		Pair(v, k.second.replace(Regex("(?<=L)[^;]+(?=;)"), { this[it.value]?.name ?: it.value })) to k.first
	})
}

fun getInheritance(paths : Collection<File>) : Map<String, List<String>> {
	val supers = mutableMapOf<String, List<String>>()
	for(path in paths)
		ZipInputStream(path.inputStream()).use { zip ->
			generateSequence{zip.nextEntry}.filter{it.name.endsWith(".class")}.forEach {
				ClassReader(zip.readBytes()).accept(object : ClassVisitor(Opcodes.ASM7) {
					override fun visit(v : Int, a : Int, name : String, sig : String?, sup : String?, int : Array<String>) {
						supers[name] = (if(sup in setOf(null, "java/lang/Object")) listOf() else listOf(sup!!)) + int.toList()
					}
				}, -1)
			}
		}
	return supers
}

open class Deobfuscate : DefaultTask() {
	@InputFile val injar = project.objects.fileProperty()
	@InputFiles @CompileClasspath val inheritance = project.objects.fileCollection()
	@InputFile val mappings = project.objects.fileProperty()
	@OutputFile val outjar = fileProperty{injar.get().asFile.name.replaceAfterLast(".", "deobf.jar")}

	@TaskAction fun run() {
		val mappings = parseMappings(mappings.get().asFile).reverse()
		val inheritance = getInheritance(inheritance.files)

		fun lookup(cls : String, name : String, desc : String) : String? =
			mappings[cls]?.run{ members[Pair(name, desc)] }
			?: inheritance[cls]?.mapNotNull { lookup(it, name, desc) }?.firstOrNull()

		remapJar(injar.get().asFile, outjar.get().asFile, object : Remapper() {
			override fun mapFieldName(cls : String, name : String, desc : String) = lookup(cls, name, desc) ?: name
			override fun mapMethodName(cls : String, name : String, desc : String) = lookup(cls, name, desc) ?: name
			override fun map(cls : String) = mappings[cls]?.run{name} ?: cls
		})
	}
}

open class Abbreviate : DefaultTask() {
	@InputFile val injar = project.objects.fileProperty()
	@Input val reverse = project.objects.property(Boolean::class.java).convention(false)
	@OutputFile val outjar = fileProperty{injar.get().asFile.name.replaceAfterLast(".", "abbrev.jar")}

	@TaskAction fun run() {
		if(outjar.get().asFile.exists()) return

		val from = if(reverse.get()) "mc/" else "net/minecraft/"
		val to = if(reverse.get()) "net/minecraft/" else "mc/"

		remapJar(injar.get().asFile, outjar.get().asFile, object : Remapper() {
			override fun map(cls : String) = if(cls.startsWith(from)) to + cls.removePrefix(from) else cls
		})
	}
}

fun remapJar(infile : File, outfile : File, mapper : Remapper) {
	ZipInputStream(infile.inputStream()).use { zip ->
		ZipOutputStream(outfile.outputStream()).use { ozip ->
			generateSequence{zip.nextEntry}.forEach {
				if(it.name.endsWith(".RSA")) {}
				else if(it.name.endsWith(".class")) {
					ozip.putNextEntry(ZipEntry(mapper.map(it.name.removeSuffix(".class"))+".class"))
					ozip.write(ClassWriter(0).apply {
						ClassReader(zip.readBytes()).accept(ClassRemapper(this, mapper), 0)
					}.toByteArray())
					ozip.closeEntry()
				} else {
					ozip.putNextEntry(it)
					zip.copyTo(ozip)
					ozip.closeEntry()
				}
			}
		}
	}
}

val rxComment = Regex("#.*")
val rxCL = Regex("(\\S+) -> (\\S+):")
val rxFD = Regex("    (\\S+) (\\S+)(?<!\\)) -> (\\S+)")
val rxMD = Regex("    (?:\\d+:\\d+:)?(\\S+) (\\S+)\\((\\S*)\\) -> (\\S+)")

fun parseMappings(file : File) : Mappings {
	fun sl(s : String) = s.replace('.', '/')
	fun type(s : String) : String =
		if(s.endsWith("[]")) "[" + type(s.removeSuffix("[]"))
		else when(s) {
			"void" -> "V"; "boolean" -> "Z"; "char" -> "C"
			"byte" -> "B"; "short" -> "S"; "int" -> "I"; "long" -> "J"
			"float" -> "F"; "double" -> "D"
			else -> "L" + sl(s) + ";"
		}

	val cls = mutableMapOf<String, Mapping>()
	lateinit var mems : MutableMap<Pair<String, String>, String>
	file.readLines().forEach { line ->
		rxComment.matchEntire(line)?.destructured ?:
		rxCL.matchEntire(line)?.destructured?.also { (name, obf) ->
			cls[sl(name)] = Mapping(sl(obf), run{mems=mutableMapOf();mems})
		} ?:
		rxFD.matchEntire(line)?.destructured?.also { (t, name, obf) ->
			mems[Pair(name, type(t))] = obf
		} ?:
		rxMD.matchEntire(line)?.destructured?.also { (ret, name, args, obf) ->
			mems[Pair(name, args.takeIf{it.any()}?.split(",").orEmpty().joinToString("", "(", ")", transform=::type) + type(ret))] = obf
		} ?:
		throw IllegalArgumentException(line)
	}
	return cls
}
