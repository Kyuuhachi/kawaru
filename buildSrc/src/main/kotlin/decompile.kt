import org.gradle.api.*
import org.gradle.api.tasks.*
import org.gradle.api.file.*

import java.io.File
import java.io.PrintWriter
import java.util.zip.ZipInputStream
import java.util.zip.ZipFile

import org.objectweb.asm.*
import org.objectweb.asm.util.*

import java.net.URLClassLoader

import org.benf.cfr.reader.api.*
import org.benf.cfr.reader.api.OutputSinkFactory.*
import org.benf.cfr.reader.bytecode.analysis.parse.utils.Pair as CfrPair

import kawaru.util.nio.*

open class Disassemble : DefaultTask() {
	@InputFile val injar = project.objects.fileProperty()
	@OutputDirectory val outdir = project.objects.directoryProperty()

	@TaskAction fun run() {
		if(outdir.isPresent) outdir.get().asFile.deleteRecursively()

		ZipInputStream(injar.get().asFile.inputStream()).use { zip ->
			generateSequence{zip.nextEntry}.filter{it.name.endsWith(".class")}.forEach { ent ->
				val bytes = ClassWriter(0).apply {
					ClassReader(zip.readBytes()).accept(object : ClassVisitor(Opcodes.ASM7, this) {
						override fun visitMethod(access: Int, name: String, desc: String, sig: String?, exc: Array<String>?)
							= VariableNamer(Opcodes.ASM7, super.visitMethod(access, name, desc, sig, exc))
					}, 0)
				}.toByteArray()

				val outf = outdir.file(ent.name.replaceAfterLast('.', "jbc")).get().path
				outf.parent.mkdirs()
				ClassReader(bytes).accept(TraceClassVisitor(PrintWriter(outf.writer())), 0)
			}
		}
	}
}

open class Decompile : DefaultTask() {
	@InputFile val injar = project.objects.fileProperty()
	@InputFiles @CompileClasspath val libs = project.objects.fileCollection()
	@OutputDirectory val outdir = project.objects.directoryProperty()

	@TaskAction fun run() {
		if(outdir.isPresent) outdir.get().asFile.deleteRecursively()

		val classes = mutableMapOf<String, ByteArray>()
		ZipInputStream(injar.get().asFile.inputStream()).use { zip ->
			generateSequence{zip.nextEntry}.filter{!it.isDirectory}.forEach { ent ->
				if(ent.name.startsWith("META-INF/")) {}
				else if(!ent.name.endsWith(".class")) {
					val outf = outdir.file(ent.name).get().path
					outf.parent.mkdirs()
					zip.copyTo(outf.outputStream())
				} else {
					classes[ent.name] = ClassWriter(0).apply {
						ClassReader(zip.readBytes()).accept(object : ClassVisitor(Opcodes.ASM7, this) {
							override fun visitMethod(access: Int, name: String, desc: String, sig: String?, exc: Array<String>?)
								= VariableNamer(Opcodes.ASM7, super.visitMethod(access, name, desc, sig, exc))
						}, 0)
					}.toByteArray()
				}
			}
		}

		val libs = libs.map(::ZipFile) // TODO make sure to close them

		CfrDriver.Builder()
			.withClassFileSource(object : ClassFileSource {
				override fun informAnalysisRelativePathDetail(usePath: String?, classFilePath: String?) {}
				override fun addJar(jarPath: String) = throw UnsupportedOperationException()
				override fun getPossiblyRenamedPath(s: String) = s
				override fun getClassFileContent(s: String) =
					CfrPair.make(classes.getOrPut(s) {
						runCatching {
							java.net.URL("jrt:/java.base/$s").readBytes()
						}.recoverCatching {
							libs.mapNotNull{it.getEntry(s)?.let(it::getInputStream)}.first().readBytes()
						}.getOrThrow()
					}, s)
			})
			.withOutputSink(object : OutputSinkFactory {
				override fun getSupportedSinks(sinkType: SinkType, collection: Collection<SinkClass>) = collection.toList()
				override fun <T> getSink(sinkType: SinkType, sinkClass: SinkClass) = Sink<T> { x ->
					if(x is SinkReturns.Decompiled) {
						val s = x.java
							.replace(Regex("^/\\*\n[^/]+\n \\*/\n"), "")
							.replace(Regex("^(    )+", RegexOption.MULTILINE), { it.value.replace("    ", "\t") })
						val outf = outdir.file("${x.packageName.replace('.', '/')}/${x.className}.java").get().path
						outf.parent.mkdirs()
						outf.writeText(s)
					} else if(x is String) logger.lifecycle(x)
					else logger.warn("Unknown event: $x")
				}
			})
			.withOptions(mapOf(
				"hideutf" to "false",
				"lomem" to "true"
			))
			.build()
			.analyse(classes.keys.toList().filter {
				// Runs out of memory trying to decompile those two classes
				it != "mc/world/item/Items.class" &&
				it != "mc/world/level/block/Blocks.class"
			} )
	}
}

val typeNames = mapOf(
	"java/lang/Object" to "o",
	"java/lang/String" to "s",

	"java/util/List" to "List",
	"java/util/ArrayList" to "List",
	"java/util/LinkedList" to "List",

	"java/util/Set" to "Set",
	"java/util/SortedSet" to "Set",
	"java/util/NavigableSet" to "Set",
	"java/util/HashSet" to "Set",
	"java/util/LinkedHashSet" to "Set",
	"java/util/TreeSet" to "Set",

	"java/util/Map" to "Map",
	"java/util/SortedMap" to "Map",
	"java/util/NavigableMap" to "Map",
	"java/util/HashMap" to "Map",
	"java/util/LinkedHashMap" to "Map",
	"java/util/LinkedTreeMap" to "Map",

	"mc/util/math/BlockPos" to "Pos",
	"mc/util/block/BlockState" to "Block",
	"mc/util/math/BlockPos\$MutableBlockPos" to "Pos",
	"mc/util/math/AxisAlignedBB" to "bb",
	"mc/item/ItemStack" to "is",

	"com/mojang/blaze3d/vertex/PoseStack" to "p"
)

class VariableNamer(api: Int, mv: MethodVisitor?) : MethodVisitor(api, mv) {
	constructor(api: Int) : this(api, null)
	override fun visitLocalVariable(name: String, desc: String, sig: String?, start: Label, end: Label, idx: Int) {
		super.visitLocalVariable(name.takeUnless { it == "☃" } ?: run {
			val arr = desc.count {it == '['}
			val desc2 = desc.drop(arr)
			val name2 =
				if(desc2.length == 1) desc2.toLowerCase()
				else {
					require(desc2[0] == 'L')
					desc2.drop(1).dropLast(1)
						.let { typeNames[it] ?: it.substringAfterLast('/').substringAfterLast('$') }
						.let { if(arr == 0) it.take(1).toLowerCase() + it.drop(1) else it }
				}
			"a".repeat(arr) + name2 + idx
		}, desc, sig, start, end, idx)
	}
}
