@file:Suppress("NOTHING_TO_INLINE")
package kawaru.util.nio

import java.io.InputStream
import java.io.IOException
import java.io.OutputStream
import java.nio.charset.Charset
import java.nio.file.CopyOption
import java.nio.file.Files
import java.nio.file.LinkOption
import java.nio.file.OpenOption
import java.nio.file.Path
import java.nio.file.attribute.BasicFileAttributes
import java.nio.file.attribute.FileAttribute
import java.nio.file.attribute.FileAttributeView
import java.nio.file.attribute.FileTime
import java.nio.file.attribute.PosixFilePermission

import kotlin.streams.asSequence

inline fun Path.copyTo(os: OutputStream) = Files.copy(this, os)
inline fun Path.copyTo(target: Path, vararg copyOptions: CopyOption) = Files.copy(this, target, *copyOptions)
inline fun Path.copyFrom(ins: InputStream, vararg opt: CopyOption) = Files.copy(ins, this, *opt)
inline fun Path.copyRecursively(target: Path, overwrite: Boolean = false,
	crossinline onError: (Path, IOException) -> OnErrorAction = { _, exception -> throw exception }
) = toFile().copyRecursively(target.toFile(), overwrite) { file, t -> onError(file.toPath(), t) }
inline fun Path.move(target: Path, vararg opt: CopyOption) = Files.move(this, target, *opt)
inline fun Path.delete() = Files.delete(this)
inline fun Path.deleteIfExists() = Files.deleteIfExists(this)
inline fun Path.deleteRecursively() = toFile().deleteRecursively()

inline fun Path.mkdir(vararg attr: FileAttribute<*>)  = Files.createDirectory(this, *attr)
inline fun Path.mkdirs(vararg attr: FileAttribute<*>) = Files.createDirectories(this, *attr)
inline fun Path.create(vararg attr: FileAttribute<*>) = Files.createFile(this, *attr)
inline fun Path.symlink(link: Path, vararg attr: FileAttribute<*>) = Files.createSymbolicLink(this, link, *attr)
inline fun Path.hardLink(link: Path) = Files.createLink(this, link)
inline fun Path.list() = Files.list(this).asSequence()

inline val Path.exists get() = Files.exists(this)
inline fun Path.exists(vararg opt: LinkOption) = Files.exists(this, *opt)
inline val Path.notExists get() = Files.notExists(this)
inline fun Path.notExists(vararg opt: LinkOption) = Files.notExists(this, *opt)

inline val Path.isDirectory get() = Files.isDirectory(this)
inline fun Path.isDirectory(vararg opt: LinkOption) = Files.isDirectory(this, *opt)
inline val Path.isRegularFile get() = Files.isRegularFile(this)
inline fun Path.isRegularFile(vararg opt: LinkOption) = Files.isRegularFile(this, *opt)

inline val Path.isSymbolicLink get() = Files.isSymbolicLink(this)
inline val Path.isExecutable get() = Files.isExecutable(this)
inline val Path.isHidden get() = Files.isHidden(this)
inline val Path.isReadable get() = Files.isReadable(this)
inline val Path.isWritable get() = Files.isWritable(this)

inline fun                                   Path.getAttribute(attr: String, vararg opts: LinkOption) = Files.getAttribute(this, attr, *opts)
inline fun <reified V : FileAttributeView>   Path.getFileAttributeView(vararg opts: LinkOption) = Files.getFileAttributeView(this, V::class.java, *opts)
inline fun <reified A : BasicFileAttributes> Path.readAttributes(vararg opts: LinkOption) = Files.readAttributes(this, A::class.java, *opts)
inline fun                                   Path.readAttributes(attrs: String, vararg opts: LinkOption) = Files.readAttributes(this, attrs, *opts)
inline fun                                   Path.setAttribute(attr: String, value: Any, vararg opts: LinkOption) = Files.setAttribute(this, attr, value, *opts)

inline var Path.lastModifiedTime
	get() = Files.getLastModifiedTime(this)
	set(v) { Files.setLastModifiedTime(this, v) }
inline fun Path.lastModifiedTime(vararg opt: LinkOption) = Files.getLastModifiedTime(this, *opt)

inline var Path.posixFilePersmissions
	get() = Files.getPosixFilePermissions(this)
	set(v) { Files.setPosixFilePermissions(this, v) }
inline fun Path.posixFilePersmissions(vararg opt: LinkOption) = Files.getPosixFilePermissions(this, *opt)

inline val Path.owner get() = Files.getOwner(this)
inline fun Path.owner(vararg opt: LinkOption) = Files.getOwner(this, *opt)
inline val Path.size get() = Files.size(this)
inline val Path.contentType get() = Files.probeContentType(this)
inline val Path.fileStore get() = Files.getFileStore(this)
inline fun Path.readSymbolicLink() = Files.readSymbolicLink(this)

inline fun Path.newByteChannel(vararg opts: OpenOption) = Files.newByteChannel(this, *opts)
inline fun Path.newByteChannel(opts: Set<OpenOption>, vararg attr: FileAttribute<*>) = Files.newByteChannel(this, opts, *attr)

inline fun Path.inputStream(vararg openOptions: OpenOption) = Files.newInputStream(this, *openOptions)
inline fun Path.outputStream(vararg openOptions: OpenOption) = Files.newOutputStream(this, *openOptions)
inline fun Path.reader(charset: Charset = Charsets.UTF_8) = Files.newBufferedReader(this, charset)
inline fun Path.writer(charset: Charset = Charsets.UTF_8, vararg openOptions: OpenOption) = Files.newBufferedWriter(this, charset, *openOptions)

inline fun Path.readLines(charset: Charset = Charsets.UTF_8) = Files.readAllLines(this, charset)
inline fun Path.writeLines(lines: Iterable<CharSequence>, charset: Charset = Charsets.UTF_8, vararg openOptions: OpenOption) = Files.write(this, lines, charset, *openOptions)
inline var Path.lines
	get() = readLines().toList()
	set(v) { writeLines(v) }

inline fun Path.readBytes() = Files.readAllBytes(this)
inline fun Path.writeBytes(array: ByteArray, vararg openOptions: OpenOption) = Files.write(this, array, *openOptions)
inline var Path.bytes
	get() = readBytes()
	set(v) { writeBytes(v) }

inline fun Path.readText(charset: Charset = Charsets.UTF_8) = bytes.toString(charset)
inline fun Path.writeText(text: String, charset: Charset = Charsets.UTF_8) { bytes = text.toByteArray(charset) }
inline var Path.text
	get() = readText()
	set(v) { writeText(v) }

inline fun Path.appendBytes(array: ByteArray) = toFile().appendBytes(array)
inline fun Path.appendText(text: String, charset: Charset = Charsets.UTF_8) = toFile().appendText(text, charset)

inline val Path.name get() = fileName.toString()
inline val Path.extension get() = toFile().extension
inline val Path.basename get() = toFile().nameWithoutExtension
inline fun Path.isSameFile(other: Path) = Files.isSameFile(this, other)
inline val Path.isRooted get() = toFile().isRooted
inline fun Path.relativeTo(other: Path) = toFile().relativeTo(other.toFile()).toPath()
inline fun Path.relativeToOrNull(other: Path) = toFile().relativeToOrNull(other.toFile())?.toPath()
inline fun Path.relativeToOrSelf(other: Path) = toFile().relativeToOrSelf(other.toFile()).toPath()

// Currently no newDirectoryStream() or walk()
