import org.gradle.api.*
import org.gradle.api.tasks.*
import org.gradle.api.provider.Provider
import org.gradle.api.file.RegularFile
import org.gradle.kotlin.dsl.*

import java.net.URL

import com.fasterxml.jackson.annotation.*
import com.fasterxml.jackson.module.kotlin.*

import kawaru.util.nio.*

data class VersionManifest(
	val latest: Latest,
	val versions: List<Version>
) {
	data class Latest(
		val release: String,
		val snapshot: String
	)
	data class Version(
		val id: String,
		val type: String,
		val url: String,
		val time: String,
		val releaseTime: String
	) {
		override fun toString() = "${type} ${id}"
	}
	val index by lazy { versions.mapIndexed { i, v -> v.id to i }.associate{it} }
}

@JsonIgnoreProperties("arguments", "logging", "javaVersion")
data class Spec( // {{{
	val assetIndex: AssetIndex,
	val assets: String,
	val id: String,
	val downloads: Downloads,
	val libraries: List<Library>,
	val mainClass: String,
	val minimumLauncherVersion: Int,
	val releaseTime: java.util.Date,
	val time: java.util.Date,
	val type: VersionType,
	val complianceLevel: Int
) {
	data class AssetIndex(
		val id: String,
		val sha1: String,
		val size: Int,
		val totalSize: Int,
		val url: String)
	data class Downloads(
		val client: Download,
		val client_mappings: Download,
		val server: Download,
		val server_mappings: Download
	) {
		data class Download(val sha1: String, val size: Int, val url: String)
	}
	data class Library(
		val name: String,
		val downloads: LibDownload,
		val natives: Map<OS, String> = mapOf(),
		val rules: List<Rule> = listOf(),
		val extract: Extract = Extract()
	) {
		data class LibDownload(val artifact: Artifact, val classifiers: Map<String, Artifact> = mapOf())
		data class Artifact(val path: String, val sha1: String, val size: Int, val url: String)
		data class Extract(val exclude: List<String> = listOf())
	}
	data class Rule(val os: OSRule?, val action: Action) {
		data class OSRule(val name: OS, val version: Regex = Regex(".*"))
		enum class Action { allow, disallow }
	}
	enum class OS {
		linux, osx, windows
	}
	enum class VersionType { snapshot, release }
} // }}}


data class AssetIndex(val objects: Map<String, Asset>) {
	data class Asset(val hash: String, val size: Int)
}

@CacheableTask
open class DownloadMC : DefaultTask() {
	@Input val version = project.objects.property<String>()

	@OutputFile val client         = fileProperty{"client-${version.get()}.jar"}
	@OutputFile val clientMappings = fileProperty{"client-${version.get()}.txt"}
	@OutputFile val server         = fileProperty{"server-${version.get()}.jar"}
	@OutputFile val serverMappings = fileProperty{"server-${version.get()}.txt"}

	@OutputDirectory val libRepo   = dirProperty{"libs"}

	// Really ugly hack. Gradle doesn't seem to contain any way to return lists.
	@OutputFile val libFile = fileProperty{"libs-${version.get()}.txt"}
	@Internal val libs = libFile.map {
		runCatching { it.path.readLines() }
			.getOrDefault(listOf())
			.map{ libRepo.file(it).get() }
			.toList()
	}

	@OutputDirectory val assets = dirProperty{"assets"}

	@TaskAction fun run() {
		val manifest = jackson.readValue<VersionManifest>(URL("https://launchermeta.mojang.com/mc/game/version_manifest.json"))
		val spec = jackson.readValue<Spec>(URL(manifest.versions.single{it.id == version.get()}.url))

		fun dl(
			url: String,
			filep: Provider<RegularFile>,
			name: String = filep.get().path.relativeTo(buildDir.get().path).toString()
		) {
			val file = filep.get().path
			if(file.exists) return
			logger.lifecycle("Downloading $name from $url")
			file.parent.mkdirs()
			URL(url).openStream().copyTo(file.outputStream())
		}

		dl(spec.downloads.client.url, client)
		dl(spec.downloads.client_mappings.url, clientMappings)
		dl(spec.downloads.server.url, server)
		dl(spec.downloads.server_mappings.url, serverMappings)

		val os = System.getProperty("os.name").toLowerCase().let { when {
			"win" in it -> Spec.OS.windows
			"mac" in it -> Spec.OS.osx
			else        -> Spec.OS.linux
		} }
		val osVersion = System.getProperty("os.version")
		fun List<Spec.Rule>.matches() =
			(none() ||
				lastOrNull {
					it.os?.run{name == os && it.os.version.matches(osVersion)} ?: true
				}?.action == Spec.Rule.Action.allow)

		val libraries = spec.libraries
			.filter{ it.rules.matches() }
			.flatMap{ listOf(it.downloads.artifact, it.downloads.classifiers[it.natives[os]]) }
			.filterNotNull()
			.map { it.url to libRepo.file(it.path) }
			.toList()
		libraries.forEach { (url, path) -> dl(url, path); }
		libFile.get().path.writeLines(libraries.map { (_, path) -> path.get().path.toString() })

		val assetIndexFile = assets.file("indexes/${version.get()}.json")
		dl(spec.assetIndex.url, assetIndexFile)
		for((k, v) in jackson.readValue<AssetIndex>(assetIndexFile.get().asFile).objects) {
			val hash = "${v.hash.take(2)}/${v.hash}"
			dl("http://resources.download.minecraft.net/${hash}", assets.file("objects/${hash}"), k)
		}
	}
}
