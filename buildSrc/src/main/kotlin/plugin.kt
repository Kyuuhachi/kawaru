import org.gradle.api.*
import org.gradle.api.provider.Provider
import org.gradle.api.file.*

import com.fasterxml.jackson.module.kotlin.*

fun Provider<Directory>.file(s : String) = map { it.file(s) }
fun Provider<Directory>.dir(s : String) = map { it.dir(s) }
val Task.buildDir get() = project.layout.buildDirectory

fun Task.fileProperty(name: ()->String) = project.objects.fileProperty()
	.convention(buildDir.map{ it.file(name()) })

fun Task.dirProperty(name: ()->String) = project.objects.directoryProperty()
	.convention(buildDir.map{ it.dir(name()) })

val RegularFile.path get() = asFile.toPath()
val Directory.path get() = asFile.toPath()

val jackson = jacksonObjectMapper()
