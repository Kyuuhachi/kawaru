plugins { mod }

dependencies {
	if(name == "kokoro") {
		implementation("kawaru:tsukuro")
		implementation("org.yaml:snakeyaml:1.24")
	} else
		implementation(project(":kokoro"))
}
