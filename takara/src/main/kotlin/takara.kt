package kawaru.takara

import java.io.File

import org.gradle.api.*
import org.gradle.api.tasks.*

import org.gradle.api.file.DuplicatesStrategy
import org.gradle.api.file.FileVisitDetails
import org.gradle.api.plugins.JavaPluginConvention

open class TakaraTask : DefaultTask() {
	@InputFiles val input = project.objects.sourceDirectorySet("takara", "takara input")
	@OutputDirectory val output = project.objects.directoryProperty()

	@TaskAction fun run() {
		project.delete(output.get().asFile.listFiles())
		val proc = Proc(output.get().asFile)
		val files = mutableListOf<FileVisitDetails>()
		input.visit { info -> files.add(info) }
		for(info in files) {
			val dir = File(info.file.path.removeSuffix(info.path))
			when {
				info.isDirectory -> {}
				info.path == "pack.yml" -> proc.pack(dir, info.path)
				info.path.endsWith(".blockstate.yml") -> proc.blockstate(dir, info.path)
				info.path.endsWith(".lang.yml") -> proc.lang(dir, info.path)
				info.path.endsWith(".yml") -> proc.generic(dir, info.path)
				else -> throw IllegalArgumentException("Can't handle ${info.path}")
			}
		}
	}
}

class TakaraPlugin : Plugin<Project> {
	override fun apply(project : Project) {
		project.pluginManager.apply("java-base")
		project.convention.getPlugin(JavaPluginConvention::class.java).sourceSets.all { sourceSet ->
			val task = project.tasks.create(sourceSet.getTaskName(null, "takara"), TakaraTask::class.java)
			task.input.srcDirs(sourceSet.resources.sourceDirectories)
			task.input.include("**/*.yml")
			sourceSet.resources.exclude("**/*.yml")
			task.output.set(project.layout.buildDirectory.dir("takara/${sourceSet.name}"))
			val processResources = project.tasks.getByName(sourceSet.processResourcesTaskName) as Copy
			processResources.from(task.output)
			processResources.duplicatesStrategy = DuplicatesStrategy.FAIL
		}
	}
}
