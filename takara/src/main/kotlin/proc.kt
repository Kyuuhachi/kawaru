package kawaru.takara

import java.io.ByteArrayOutputStream
import java.io.File
import java.math.BigDecimal
import java.math.BigInteger

import com.fasterxml.jackson.core.JsonGenerator
import com.fasterxml.jackson.core.JsonFactory

import org.yaml.snakeyaml.composer.Composer
import org.yaml.snakeyaml.error.Mark
import org.yaml.snakeyaml.nodes.*
import org.yaml.snakeyaml.parser.ParserImpl
import org.yaml.snakeyaml.reader.StreamReader
import org.yaml.snakeyaml.resolver.Resolver

typealias Loc = () -> String

sealed class Yaml {
	abstract val tag : String
	abstract val loc : Loc
	abstract fun accept(f : Yaml.() -> Yaml) : Yaml
	fun addLoc(l : Loc) : Yaml = accept{addLoc(l)}.withLoc({loc()+l()})
	abstract fun withLoc(l : Loc) : Yaml
}
data class Scalar(override val tag : String, val value : String, override val loc : Loc) : Yaml() {
	override fun accept(f : Yaml.() -> Yaml) = this
	override fun withLoc(l : Loc) = copy(loc=l)
}
data class Sequence(override val tag : String, val value : List<Yaml>, override val loc : Loc) : Yaml() {
	override fun accept(f : Yaml.() -> Yaml) = copy(value=value.map{ f(it) })
	override fun withLoc(l : Loc) = copy(loc=l)
}
data class Mapping(override val tag : String, val value : List<Pair<Yaml, Yaml>>, override val loc : Loc) : Yaml() {
	override fun accept(f : Yaml.() -> Yaml) = copy(value=value.map{ (k, v) -> f(k) to f(v) })
	override fun withLoc(l : Loc) = copy(loc=l)
}

private val Node.yaml get() : Yaml = when(this) {
	is ScalarNode -> Scalar(tag.string, value, loc(startMark, endMark))
	is SequenceNode -> Sequence(tag.string, value.map{it.yaml}, loc(startMark, endMark))
	is MappingNode -> Mapping(tag.string, value.map{it.keyNode.yaml to it.valueNode.yaml}, loc(startMark, endMark))
	else -> throw IllegalArgumentException()
}

private fun loc(s : Mark, e : Mark) = {
	require(s.buffer === e.buffer)
	val buf = s.buffer
	val startLine = run {
		var i = s.pointer
		while(i >= 0 && buf[i] != 0x0A) i--
		i+1
	}
	StringBuilder().apply{
		append("\nat ${s.name}@${s.line+1}:${s.column+1}: ")
		for(i in startLine..buf.size) {
			if(buf[i] == 0x0A) break
			if(i == s.pointer) append("\u001B[1;31m")
			if(i == e.pointer) append("\u001B[0m")
			appendCodePoint(buf[i])
		}
		append("\u001B[0m")
	}.toString()
}
private val Tag.string get() = if(startsWith(Tag.PREFIX)) "!!" + value.substring(Tag.PREFIX.length) else value

class Proc(val outDir : File) {
	fun generic(dir : File, path : String) {
		json(path.replaceAfterLast('.', "json")) { json ->
			yaml(dir, path).toJson(json)
		}
	}

	fun pack(dir : File, path : String) {
		json("pack.mcmeta") { json ->
			yaml(dir, path).toJson(json)
		}
	}

	fun lang(dir : File, path : String) {
		json(path.replaceAfter('.', "json")) { json ->
			json.writeStartObject()
			yaml(dir, path).parseLang(json::writeStringField)
			json.writeEndObject()
		}
	}

	fun blockstate(dir : File, path : String) {
		json(path.replaceAfter('.', "json")) { json ->
			json.writeStartObject()
			json.writeArrayFieldStart("multipart")
			yaml(dir, path).parseBlockState(json)
			json.writeEndArray()
			json.writeEndObject()
		}
	}

	private fun yaml(dir : File, path : String) =
		Composer(
			ParserImpl(StreamReader(File(dir, path).reader()).apply{
				this::class.java.getDeclaredField("name").apply{isAccessible=true}.set(this, path)
			}),
			Resolver().apply{
				addImplicitResolver(Tag(Tag.PREFIX+"value"), Resolver.VALUE, "=")
			}
		).singleNode.yaml.eval(mapOf()).merge()

	private fun json(path : String, body : (JsonGenerator) -> Unit) {
		val f = File(outDir, path)
		require(!f.exists(), {"Duplicate file $path"})
		f.parentFile.mkdirs()
		f.writer().use {
			JsonFactory().createGenerator(it).let { json ->
				json.enable(JsonGenerator.Feature.STRICT_DUPLICATE_DETECTION)
				body(json)
				json.writeRaw('\n')
				json.close()
			}
		}
	}

	private fun auxjson(path : String, body : (JsonGenerator) -> Unit) : String {
		val bytes = ByteArrayOutputStream()
		bytes.writer().use {
			JsonFactory().createGenerator(it).let { json ->
				json.enable(JsonGenerator.Feature.STRICT_DUPLICATE_DETECTION)
				body(json)
				json.writeRaw('\n')
				json.close()
			}
		}
		val hash = org.apache.commons.codec.digest.DigestUtils.sha1Hex(bytes.toByteArray())
		val f = File(outDir, path.replace("{}", hash))
		f.parentFile.mkdirs()
		f.writeBytes(bytes.toByteArray())
		return hash
	}

	private fun Yaml.toJson(json : JsonGenerator) : Unit = when(this) {
		is Mapping -> {
			json.writeStartObject()
			for((k, v) in value) {
				require(k is Scalar, {"key must be scalar" + k.loc()})
				json.writeFieldName(k.value)
				v.toJson(json)
			}
			json.writeEndObject()
		}
		is Sequence -> {
			json.writeStartArray()
			for(v in value)
				v.toJson(json)
			json.writeEndArray()
		}
		is Scalar -> when(tag) {
			"!!int" -> json.writeNumber(BigInteger(value)) // TODO parse numbers better
			"!!float" -> json.writeNumber(BigDecimal(value))
			"!!bool" -> json.writeBoolean(when(value.toLowerCase()) {
				"y", "yes", "true", "on" -> true
				"n", "no", "false", "off" -> false
				else -> throw IllegalArgumentException("illegal !!bool${loc()}")
			})
			"!!null" -> json.writeNull()
			"!!str" -> json.writeString(value)
			else -> throw IllegalArgumentException("unknown tag" + loc())
		}
	}

	private fun Yaml.merge() : Yaml = when {
		tag == "!var" -> {
			require(this is Scalar, {"!var must be scalar" + loc()})
			throw IllegalArgumentException("unresolved !var" + loc())
		}
		tag == "!join" -> {
			require(this is Sequence, {"!join must be a sequence" + loc()})
			Scalar("!!str", value.map{it.merge()}.map{
				require(it is Scalar, {"!join value must be scalar" + it.loc()})
				it.value
			}.joinToString(""), loc)
		}
		this is Mapping -> copy(value=value.flatMap{ (k, v) -> when {
			k.tag == "!def" -> listOf()
			k.tag == "!!merge" -> {
				when(v) { is Sequence -> v.value; else -> listOf(v) }.map{it.merge()}.flatMap {
					require(it is Mapping, {"!!merge value must be a mapping" + it.loc()})
					it.value
				}
			}
			else -> listOf(k.merge() to v.merge())
		} })
		else -> accept{merge()}
	}

	private fun Yaml.eval(vars : Map<String, Yaml>) : Yaml = when {
		tag == "!var" -> {
			require(this is Scalar, {"!var must be scalar" + loc()})
			vars[value] ?: this
		}
		tag == "!subst" -> {
			require(this is Mapping, {"!subst must be a mapping" + loc()})
			var template = null as Yaml?
			val args = mutableMapOf<String, Yaml>()
			for((k, v) in value) {
				if(k.tag == "!!value") {
					require(template == null, {"duplicate !subst template" + v.loc()})
					template = v
				} else {
					require(k is Scalar, {"!subst argument must be scalar" + k.loc()})
					require(k.value !in args, {"duplicate !subst argument ${k.value}" + k.loc()})
					args[k.value] = v
				}
			}
			require(template != null, {"no !subst template" + loc()})
			template.eval(args).eval(vars).addLoc(loc)
		}
		else -> accept{eval(vars)}
	}

	private fun Yaml.parseLang(callback : (String, String) -> Unit) {
		data class LangKey(val ns : String? = null, val parts : List<String> = listOf()) {
			override fun toString() = (if(ns == null) "" else ns+":") + parts.joinToString(".")
			operator fun div(o : String) = copy(parts = parts + o)
		}
		fun Yaml.recurse(path : LangKey) : Unit = when(this) {
			is Scalar -> {
				require(path.parts.any(), {"can't have empty key" + loc()})
				callback(path.toString(), value)
			}
			is Sequence -> throw IllegalArgumentException("unexpected sequence" + loc())
			is Mapping ->
				for((k, v) in value) {
					require(k is Scalar, {"key must be string" + loc()})
					when(k.tag) {
						"!namespace" -> {
							require(path == LangKey(), {"!namespace can only be set on top level" + k.loc()})
							require(k.value matches Regex("[a-z0-9_.-]*"), {"invalid namespace" + k.loc()})
							require(v is Mapping, {"!namespace must be mapping" + v.loc()})
							v.recurse(path.copy(ns=k.value))
						}
						"!!value" -> {
							require(v is Scalar, {"!!value must have scalar value" + v.loc()})
							v.recurse(path)
						}
						else -> v.recurse(path / k.value)
					}
				}
			}

		recurse(LangKey())
	}

	private fun Yaml.parseBlockState(json : JsonGenerator, state : Map<String, Scalar> = mapOf()) : Unit = when {
		this is Sequence ->
			for(v in value) {
				json.writeStartObject()
				if(state.any()) {
					json.writeObjectFieldStart("when")
					for((k2, v2) in state) { json.writeFieldName(k2); v2.toJson(json) }
					json.writeEndObject()
				}
				json.writeFieldName("apply")
				if(v is Sequence) {
					require(v.value.any(), {"empty random choice" + v.loc()})
					json.writeStartArray()
					v.value.forEach{ v2 -> v2.parseBlockStateApply(json) }
					json.writeEndArray()
				} else v.parseBlockStateApply(json)
				json.writeEndObject()
			}
		this is Mapping ->
			for((k, v) in value) {
				require(k is Mapping, {"key must be mapping" + k.loc()})
				val state2 = mutableMapOf<String, Scalar>()
				for((k2, v2) in k.value) {
					require(k2 is Scalar, {"state key must be scalar" + k2.loc()})
					require(v2 is Scalar, {"state value must be scalar" + v2.loc()})
					require(k2.value !in state2, {"duplicate state key" + k2.loc()})
					state2[k2.value] = v2
				}
				v.parseBlockState(json, state + state2)
			}
		else -> throw IllegalArgumentException("invalid type" + loc())
	}

	private fun Yaml.parseBlockStateApply(json : JsonGenerator) {
		val apply = mutableMapOf<String, Yaml>()
		val gen = mutableMapOf<String, Yaml>()
		when {
			this is Scalar -> apply["model"] = this
			this is Mapping ->
				for((k, v) in value) {
					require(k is Scalar, {"apply key must be scalar" + k.loc()})
					val (key, map) = if(k.tag == "!gen") {
						require(k.value != "parent", {"can't set 'parent'" + k.loc()})
						Pair(k.value, gen)
					} else {
						require(k.value != "model", {"can't set 'model' (use !!value)" + k.loc()})
						Pair(if(k.tag == "!!value") "model" else k.value, apply)
					}
					require(key !in map, {"duplicate key" + k.loc()})
					map[key] = v
				}
			else -> throw IllegalArgumentException()
		}
		if(gen.any()) {
			if("model" in apply)
				gen["parent"] = apply["model"]!!
			val hash = auxjson("assets/sha1/models/{}.json") { aux ->
				aux.writeStartObject()
				for((k, v) in gen) { aux.writeFieldName(k); v.toJson(aux) }
				aux.writeEndObject()
			}
			apply["model"] = Scalar("!!str", "sha1:$hash", loc)
		}
		json.writeStartObject()
		for((k, v) in apply) { json.writeFieldName(k); v.toJson(json) }
		json.writeEndObject()
	}
}
